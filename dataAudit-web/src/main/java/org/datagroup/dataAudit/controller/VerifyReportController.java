package org.datagroup.dataAudit.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.common.util.VerifyDataCache;
import org.datagroup.dataAudit.service.impl.VerifyRepairService;
import org.datagroup.dataAudit.service.impl.VerifyReportService;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;
import org.datagroup.dataAudit.thread.ReVerifyThread;
import org.datagroup.dataAudit.thread.ResetIncTableThread;
import org.datagroup.dataAudit.thread.VerifyTaskThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

@Controller
public class VerifyReportController {
  private static final Logger logger = LogManager.getLogger(VerifyReportController.class);

  @Autowired
  private VerifyReportService iVerifyReportApi;
  
  @Autowired
  private VerifyRepairService iVerifyRepairApi;
  
  @Autowired
  private VerifyTaskService iTbVerifyTaskApi;

  @RequestMapping("//findLog")
  public String find() {
    return "verifyApp/verifyReport/verifyReport";
  }

  @RequestMapping("//findALLLog")
  public String findALLLog() {
    return "verifyApp/verifyReport/verifyLog";
  }
  
  
  @RequestMapping("/initLogDetail")
  public String initLogDetail(@ModelAttribute TbVerifyLog tbVerifyLog,ModelMap modelMap){
	  	modelMap.put("tbVerifyLog", tbVerifyLog);
	  return "verifyApp/verifyReport/verifyReportDetail";
  }
  
 
  @RequestMapping("//findRepairTask")
  public String findRepair() {
    return "verifyApp/verifyReport/verifyReportTask";
  }
  
  
  
  /**
   * 【】
   */
  @ResponseBody
  @RequestMapping("/findLogGroupBy")
  public String findLogGroupBy(@ModelAttribute TbVerifyLog tbVerifyLog) {
    return iVerifyReportApi.findLogGroupBy("", tbVerifyLog).toString();
  }

  @ResponseBody
  @RequestMapping("/findDetail")
  public String findDetail(@ModelAttribute TbVerifyLog tbVerifyLog) {
    return iVerifyReportApi.findDetail("", tbVerifyLog).toString();
  }
  
  @ResponseBody
  @RequestMapping("/findRepairDetail")
  public String findRepairGroupBy(@ModelAttribute TbVerifyRepairTask tbVerifyRepairTask ){
	  return iVerifyReportApi.findRepairTask("",tbVerifyRepairTask).toString();
  }
  

  
  @ResponseBody
  @RequestMapping("/repairStartVerify")
  public String repairStartVerify(@RequestParam("repairTaskNameStr") String repairTaskNameStr) {
	  JSONObject resultObject = new JSONObject();
	  if(repairTaskNameStr!=null&&!repairTaskNameStr.equals("")){
		  String[] repairStrs=repairTaskNameStr.split(",");
		  TbVerifyRepairTask paramTask=new TbVerifyRepairTask();
		  for(String item:repairStrs){
			  String[] params=item.split("#");
			  TbVerifyRepair paramRepair=new TbVerifyRepair();
			  paramRepair.setSrc_table(params[0]);
			  paramRepair.setTarget_table(params[1]);
			  paramRepair.setVerify_type(Integer.parseInt(params[2]));
			  String[] timeStrs=params[3].split("T");
			  paramRepair.setStart_time(timeStrs[0]);
			  paramRepair.setEnd_time(timeStrs[1]);
			  String threadName=paramRepair.getSrc_table()+"#"+paramRepair.getTarget_table()+"#"+paramRepair.getVerify_type()+"#"+timeStrs[0]+"T"+timeStrs[1];
			  paramTask.setThread_name(threadName);
			  iVerifyRepairApi.deleteVerifyRepairTask("",paramTask);
			  new ReVerifyThread(item+"_ReVerifyThread",paramRepair,iVerifyReportApi,iVerifyRepairApi,iTbVerifyTaskApi).start();
		  }
	  }
	  resultObject.put("code",0);
	  resultObject.put("msg", "推送成功");
	  return resultObject.toString();
  }
  
  
  
  
  @ResponseBody
  @RequestMapping("/resetInc")
  public String restIncTable(@RequestParam("verifyIds") String verifyIds){
	  JSONObject resultObject = new JSONObject();
	  try{
		  if(verifyIds!=null&&!verifyIds.equals("")){
			  String[] verifyIdStr=verifyIds.split(",");
			  List<Integer> paramVerifyIds=new ArrayList<Integer>();
			  for(String item:verifyIdStr){
				  paramVerifyIds.add(Integer.parseInt(item));
			  }
			  List<TbVerifyConfig> verifyConfigList= iTbVerifyTaskApi.findVerifyConfgByIDs("", paramVerifyIds);
			  for(TbVerifyConfig itemConfig:verifyConfigList){
				  new ResetIncTableThread(iTbVerifyTaskApi,itemConfig).start();
			  }
		  }
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  resultObject.put("code",0);
	  resultObject.put("msg", "推送成功");
	  return resultObject.toString();
  }
  
}
