package org.datagroup.dataAudit.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.service.impl.VerifyRepairService;
import org.datagroup.dataAudit.thread.VerifyRepairThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Controller
public class VerifyRepairController {
  private static final Logger logger = LogManager.getLogger(VerifyRepairController.class);

  @Autowired
  private VerifyRepairService verifyRepairService;

  
  @RequestMapping("/findRepairPage")
  public String find() {
    return "verifyApp/verifyReport/verifyRepair";
  }
  
  
  @ResponseBody
  @RequestMapping("/doDataRepair")
  public String doDataRepair(@RequestParam("tbVerifyLogStr") String tbVerifyLogStr) {
	  JSONObject resultObject = new JSONObject();
	  try{
	      JSONArray tempArray = JSON.parseArray(tbVerifyLogStr);
	
	      List<TbVerifyLog> tbVerifyLogs = JSON.parseArray(tempArray.toJSONString(), TbVerifyLog.class);
	      TbVerifyRepairTask paramTask=new TbVerifyRepairTask();
	      for(TbVerifyLog paramItem:tbVerifyLogs){//排重
	      	if(paramItem.getVerify_type()>0){
	      		String sourceTable=paramItem.getSource_table().trim().toUpperCase();
	      		String targetTable=paramItem.getTarget_table().trim().toUpperCase();
	      		String[] timeArray=DateUtil.getDayAfter(paramItem.getLast_update()).split("#");
	      		String threadName=sourceTable+"#"+targetTable+"#"+paramItem.getVerify_type()+"#"+timeArray[0]+"T"+timeArray[1];
	      		paramTask.setThread_name(threadName);//设置线程名称
	      		paramItem.setRepair_status(0);
	      		paramItem.setStart_time(timeArray[0]);
	      		paramItem.setEnd_time(timeArray[1]);
	      		 verifyRepairService.deleteVerifyRepairTask("",paramTask);
	      		verifyRepairService.updateRepairStatusByVerifyLog("", paramItem);
	      		verifyRepairService.deleteTbRepairData("", paramItem);
	      		new VerifyRepairThread(threadName,paramItem,verifyRepairService).start();
	      	}
	      }
	      
	      resultObject.put("code",0);
    	  resultObject.put("msg", "推送成功");
	  }catch(Exception e){
		  e.printStackTrace();
		  resultObject.put("msg","推送异常!");
		  resultObject.put("code", -1);
	  }
	  return resultObject.toString();
	  
  }
  
  
  @ResponseBody
  @RequestMapping("/findRepair")
  public JSONObject findRepair(String dataSourceCode,TbVerifyRepair tbVerifyRepair){
		int num=verifyRepairService.findVerifyRepairCount(dataSourceCode, tbVerifyRepair);
		JSONObject jsonObject = new JSONObject();
	        jsonObject.put("code", 0);
	        jsonObject.put("msg", "查询成功!");
	        jsonObject.put("count",num);
	        List<TbVerifyRepair> tbVerifyRepairs=verifyRepairService.findVerifyRepair(dataSourceCode, tbVerifyRepair);
	     jsonObject.put("data", tbVerifyRepairs);
		return jsonObject;
	}
  

  
  
}
