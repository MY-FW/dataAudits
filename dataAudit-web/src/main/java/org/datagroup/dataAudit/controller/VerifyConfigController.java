package org.datagroup.dataAudit.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.service.impl.VerifyConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Http
 * 
 * @author zk
 * @creation 2018年9月28日
 */
@Controller
public class VerifyConfigController {

  private static final Logger logger = LogManager.getLogger(VerifyConfigController.class);
  @Autowired
  private VerifyConfigService verifyConfigApi;

  @RequestMapping("/finds")
  public String find() {
    return "verifyApp/verifyConfig/verifyConfigList";
  }

  @RequestMapping("findsadd")
  public String findsadd() {
    return "verifyApp/verifyConfig/verifyConfigAdd.html";

  }

	@ResponseBody
	@RequestMapping("/findConfigAll")
	public String findTbVerifyConfigList(String tbVerifyConfig) {
		JSONObject jsonObject = new JSONObject();
		List<TbVerifyConfig> tbVerifyConfigs = verifyConfigApi.findVerifyConfigPageAll(tbVerifyConfig);
		jsonObject.put("code", 0);
		jsonObject.put("msg", "查询成功!");
		jsonObject.put("count", tbVerifyConfigs.size());
		jsonObject.put("data", tbVerifyConfigs);
		logger.info("查询结果为：{}", jsonObject.toJSONString());
		return jsonObject.toJSONString();
	}

  /*
   * @RequestMapping(value="/upVerifyConfig/{verify_id}") public String
   * getTbVerifyConfigLists(@PathVariable Integer verify_id,TbVerifyConfig
   * tbVerifyConfig){ // TbVerifyConfig
   * tbVerifyConfig1=verifyConfigApi.getTbVerifyConfigList(verify_id);
   * //map.put("p",tbVerifyConfig1); JSONObject jsonObject = new JSONObject();
   * verifyConfigApi.getTbVerifyConfigList(verify_id);
   * verifyConfigApi.updateTbVerifyConfig(tbVerifyConfig); return
   * jsonObject.toJSONString(); }
   */
  /*
   * @RequestMapping(value="/VerifyConfigUpdate") public String
   * updateTbVerifyConfigs(TbVerifyConfig tbVerifyConfig){
   * verifyConfigApi.updateTbVerifyConfig(tbVerifyConfig);
   * return"verifyApp/verifyConfig/verifyConfig.html"; }
   */
	
  @RequestMapping(value = "/editVerifyConfig", method = RequestMethod.POST)
  public @ResponseBody String getTbVerifyConfigLists(TbVerifyConfig tbVerifyConfig, HttpServletRequest request, Map<String, Object> requestMap) {
    String verify_id = Integer.toString(tbVerifyConfig.getVerify_id());
    TbVerifyConfig tbVerifyConfig2 = verifyConfigApi.getTbVerifyConfigList(verify_id);
    tbVerifyConfig2.setSource_inc_table(tbVerifyConfig.getSource_inc_table());
    tbVerifyConfig2.setInc_table_count(tbVerifyConfig.getInc_table_count());
    tbVerifyConfig2.setSource_base_code(tbVerifyConfig.getSource_base_code());
    tbVerifyConfig2.setTarget_base_code(tbVerifyConfig.getTarget_base_code());
    tbVerifyConfig2.setSource_base_name(tbVerifyConfig.getSource_base_name());
    tbVerifyConfig2.setTarget_base_name(tbVerifyConfig.getTarget_base_name());
    tbVerifyConfig2.setSource_base_en_name(tbVerifyConfig.getSource_base_en_name());
    tbVerifyConfig2.setTarget_base_en_name(tbVerifyConfig.getTarget_base_en_name());
    tbVerifyConfig2.setSource_table(tbVerifyConfig.getSource_table());
    tbVerifyConfig2.setTarget_table(tbVerifyConfig.getTarget_table());
    tbVerifyConfig2.setProcess_name(tbVerifyConfig.getProcess_name());
    tbVerifyConfig2.setSource_fields(tbVerifyConfig.getSource_fields());
    tbVerifyConfig2.setTarget_fields(tbVerifyConfig.getTarget_fields());
    tbVerifyConfig2.setStart_time(tbVerifyConfig.getStart_time());
    tbVerifyConfig2.setEnd_time(tbVerifyConfig.getEnd_time());
    tbVerifyConfig2.setCreate_time(tbVerifyConfig.getCreate_time());
    tbVerifyConfig2.setAudit_status(tbVerifyConfig.getAudit_status());
	tbVerifyConfig2.setStatus(tbVerifyConfig.getStatus());
	tbVerifyConfig2.setCenter_type(Integer.valueOf(tbVerifyConfig.getCenter_type()));
	tbVerifyConfig2.setRemarks(tbVerifyConfig.getRemarks());
	verifyConfigApi.updateTbVerifyConfig(tbVerifyConfig2);
	JSONObject jsonObject = new JSONObject();
    jsonObject.put("code", 0);
    jsonObject.put("msg", "修改数据源成功!");
    return jsonObject.toString();
	
  }

  // 删除
  @RequestMapping("/tbVerifyConfigDelete")
  public @ResponseBody String tbVerifyConfigDelete(HttpServletRequest request) throws UnsupportedEncodingException {
    String id = request.getParameter("id");
    verifyConfigApi.delete(id);
    // System.out.println(id);
    // return "redirect:/informa/mohu";
    JSONObject json = new JSONObject();
    return json.toString();

  }
  @RequestMapping(value = "/TbVerifyConfigadd")
  public String TbVerifyConfigadd(@ModelAttribute("tbVerifyConfig") TbVerifyConfig tbVerifyConfig) {
    int result = verifyConfigApi.TbVerifyConfigadd(tbVerifyConfig);
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("code", result);
    jsonObject.put("msg", "添加数据源成功!");
    return jsonObject.toString();
  }
}
