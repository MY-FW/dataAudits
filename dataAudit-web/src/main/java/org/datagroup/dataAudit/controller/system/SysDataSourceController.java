package org.datagroup.dataAudit.controller.system;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.basepo.ResponseInfo;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;
import org.datagroup.dataAudit.service.impl.SysDataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @功能说明:数据源信息处理类
 * @author Chentw
 * @version
 * @版权: 版权所有 (c) 2018
 * @创建日期: 2018年12月4日
 */
@Controller
@RequestMapping("/system")
public class SysDataSourceController {
  private static final Logger logger = LogManager.getLogger(SysDataSourceController.class);

  @Autowired
  private SysDataSourceService sysDataSourceApi;

  /**
   * @Description:跳转数据源信息展示页面
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @GetMapping(value = "/showdspage")
  public String sysDataSource() {
    return "system/queryDs";
  }

  /**
   * @Description:显示数据源信息
   * @param dataSourceCode
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @ResponseBody
  @RequestMapping(value = "/queryds")
  public String queryDs(@ModelAttribute("searchParam") String searchParam) {
    ResponseInfo<List<SysDataSourceInfo>> sysDataSourceList = sysDataSourceApi.query(searchParam);
    logger.info("查询结果为：{}", sysDataSourceList.toJSONText());
    return sysDataSourceList.toJSONText();
  }

  /**
   * @Description:删除数据
   * @param dataSourceCode
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @ResponseBody
  @RequestMapping("/delds")
  public String delDs(String dsCode) {
    ResponseInfo result = sysDataSourceApi.delDs(dsCode);
    logger.info("删除结果为：[{}]", result.toJSONText());
    return result.toJSONText();
  }

  /**
   * @Description:跳转修改页面
   * @param dsInfo
   * @param model
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @RequestMapping("/showeditds")
  public String showEditDs(String baseCode, ModelMap modelMap) {
    ResponseInfo<List<SysDataSourceInfo>> result = sysDataSourceApi.query(baseCode);
    if ("0000".equals(result.getCode())) {
      modelMap.addAttribute("data", result.getData().get(0));
    } else {
      modelMap.addAttribute(result);
    }
    return "system/editDs";
  }

  /**
   * @Description:修改数据源信息
   * @param dsInfo
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @ResponseBody
  @RequestMapping("/editds")
  public String editDs(@ModelAttribute("dsInfo") SysDataSourceInfo dsInfo) {
    ResponseInfo result = sysDataSourceApi.editDs(dsInfo);
    logger.info("修改结果为：[{}]", result.toJSONText());
    return result.toJSONText();
  }

  /**
   * @Description:跳转添加页面
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @RequestMapping("/showaddpage")
  public String showAddPage() {
    return "system/addDs";
  }

  /**
   * @Description:添加数据源信息
   * @param dsInfo
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @ResponseBody
  @RequestMapping("/addds")
  public String addDs(@ModelAttribute("dsInfo") SysDataSourceInfo dsInfo) {
    ResponseInfo result = sysDataSourceApi.addDs(dsInfo);
    logger.info("执行结果为：[{}]", result.toJSONText());
    return result.toJSONText();
  }

  /**
   * @Description:加载数据源
   * @param dataSourceCode
   * @param status
   * @return
   * @return String
   * @author Chentw
   * @date 2018年12月4日
   */
  @ResponseBody
  @RequestMapping("/createds")
  public String createDs(String baseCode, String baseStatus) {
    ResponseInfo result = sysDataSourceApi.createDs(baseCode, baseStatus);
    return result.toJSONText();
  }

}
