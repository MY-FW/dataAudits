package org.datagroup.dataAudit.controller.authFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.datagroup.dataAudit.common.po.SysMenuInfo;
import org.datagroup.dataAudit.common.po.SysRoleInfo;
import org.datagroup.dataAudit.common.po.SysUserInfo;
import org.datagroup.dataAudit.service.impl.SysUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;

public class AuthRealm extends AuthorizingRealm {

  @Autowired
  ConfigurableApplicationContext applicationContext;

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
    SysUserInfoService sysUserInfoApi = applicationContext.getBean("sysUserInfoApi", SysUserInfoService.class);
    UsernamePasswordToken utoken = (UsernamePasswordToken) token;// 获取用户输入的token
    String username = utoken.getUsername();
    SysUserInfo user = sysUserInfoApi.findByParam(username, null, null);
    return new SimpleAuthenticationInfo(user, user.getPassword(), this.getClass().getName());// 放入shiro.调用CredentialsMatcher检验密码
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
    SysUserInfo user = (SysUserInfo) principal.fromRealm(this.getClass().getName()).iterator().next();// 获取session中的用户
    List<String> permissions = new ArrayList<>();
    Set<SysRoleInfo> roleInfos = user.getRoleInfos();
    if (roleInfos.size() > 0) {
      for (SysRoleInfo role : roleInfos) {
        Set<SysMenuInfo> menuInfos = role.getMenuInfos();
        if (menuInfos.size() > 0) {
          for (SysMenuInfo menuInfo : menuInfos) {
            permissions.add(menuInfo.getMenuCode());
          }
        }
      }
    }
    SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
    info.addStringPermissions(permissions);// 将权限放入shiro中.
    return info;
  }
}
