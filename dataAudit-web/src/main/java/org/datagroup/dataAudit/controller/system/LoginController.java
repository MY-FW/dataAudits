package org.datagroup.dataAudit.controller.system;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.datagroup.dataAudit.common.po.SysUserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @功能说明: 登录
 * @author Chentw
 * @version
 * @版权: 版权所有 (c) 2018
 * @创建日期: 2018年11月30日
 */
@Controller
public class LoginController {
  private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

  @RequestMapping(value = "/")
  public String login() {
    return "login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(String account, String password, HttpSession session, ModelMap modelMap) {
    UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(account, password);
    Subject subject = SecurityUtils.getSubject();
    try {
      subject.login(usernamePasswordToken); // 完成登录
      SysUserInfo user = (SysUserInfo) subject.getPrincipal();
      session.setAttribute("user", user);
      modelMap.addAttribute("userInfo", user);
      return "welcome";
    } catch (Exception e) {
      return "login";
    }
  }

  @RequestMapping(value = "/logout", method = RequestMethod.GET)
  public String logOut(HttpSession session) {
    Subject subject = SecurityUtils.getSubject();
    subject.logout();
    // session.removeAttribute("user");
    return "login";
  }

  // 注解的使用
  @RequiresRoles("admin")
  @RequiresPermissions("create")
  @RequestMapping(value = "/create")
  public String create() {
    return "Create success!";
  }
}
