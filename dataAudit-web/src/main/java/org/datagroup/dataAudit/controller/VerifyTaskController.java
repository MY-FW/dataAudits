package org.datagroup.dataAudit.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.TbVerifyTask;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.common.util.VerifyDataCache;
import org.datagroup.dataAudit.thread.initTaskThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author yannannan
 * @createTime 2018年11月30日
 * @updateTime 2018年11月30日
 * @desc
 */

@Controller
public class VerifyTaskController {
  private static final Logger logger = LogManager.getLogger(VerifyTaskController.class);

	 @Autowired
	 private VerifyTaskService iVerifyTaskApi;
	 
	  @RequestMapping("/findTask")
	  public String find() {
	    return "verifyApp/taskConfig/taskList";
	  }
	 
	 
	 /**
	  * 【查询所有稽核任务】
	  */
	 @ResponseBody
	 @RequestMapping("/findTaskAll")
	 public String findVerifyTaskList(@ModelAttribute TbVerifyTask tbVerifyTask){
		 return iVerifyTaskApi.findVerifyTaskPageAll("batchdml",tbVerifyTask).toString();
	 }
	 
	 
	 
	 
	 /**
	  * 【启动任务】
	  */
	 @ResponseBody
	 @RequestMapping("/startTask")
	 public String startVerifyTask(@ModelAttribute TbVerifyConfig tbVerifyConfig){
		 if(tbVerifyConfig!=null){
			if(tbVerifyConfig.getVerifyIds()!=null&&!tbVerifyConfig.getVerifyIds().trim().equals("")){
				String[] verifyIdsArray=tbVerifyConfig.getVerifyIds().split(",");//稽核配置ids
				List<Integer> verifyIds=new ArrayList<Integer>();
				for(String verifyItemId:verifyIdsArray){
					verifyIds.add(Integer.parseInt(verifyItemId.trim()));
				}
				List<TbVerifyConfig> executeConfig=iVerifyTaskApi.findVerifyConfgByIDs("",verifyIds);//查询到稽核配置信息
				for(TbVerifyConfig item:executeConfig){
					if(item.getAudit_status().equals("70B")||item.getAudit_status().equals("70E")||item.getAudit_status().equals("70C")){
						String threadName="task"+item.getVerify_id();
						VerifyDataCache.THREAD_STATUS_CACHE.put(threadName, false);
					}
				}
				new initTaskThread(iVerifyTaskApi,executeConfig).start();//启动任务分发线程
			} 
		 }
		 return null;
	 }
	 
	 
	 
}
