package org.datagroup.dataAudit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 该工程为独立WEB工程。
 * 
 * @author Chentw
 * @creation 2018年10月19日
 */
@EnableDubboConfiguration
@SpringBootApplication
@EnableAdminServer
public class WebApp {
  private static final Logger logger = LogManager.getLogger(WebApp.class);

  public static void main(String[] args) {
    ConfigurableApplicationContext applicationContext = SpringApplication.run(WebApp.class, args);
    // VerifyTaskService iVerifyTaskApi=(VerifyTaskService)
    // applicationContext.getBean("iTbVerifyTaskApi");
    // new InitVerifyConfig(iVerifyTaskApi).start();
    // new initTaskThread(iVerifyTaskApi,null).start();
  }

}
