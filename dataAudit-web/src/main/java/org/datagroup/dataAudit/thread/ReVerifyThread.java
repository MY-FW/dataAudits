package org.datagroup.dataAudit.thread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.util.VerifyDataCache;
import org.datagroup.dataAudit.service.impl.VerifyRepairService;
import org.datagroup.dataAudit.service.impl.VerifyReportService;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;

/**
 * 【重跑线程】
 * @author yannannan
 * @createTime 2018年12月12日
 * @updateTime 2018年12月12日
 * @desc
 */
public class ReVerifyThread extends Thread{
	private static final Logger log = LogManager.getLogger(ReVerifyThread.class);
	private String theadName;
	private TbVerifyRepair tbVerifyRepair;
	private VerifyReportService iVerifyReportApi;
	private VerifyRepairService iVerifyRepairApi;
	private VerifyTaskService iTbVerifyTaskApi;

	public ReVerifyThread(String theadName, TbVerifyRepair tbVerifyRepair,
			VerifyReportService iVerifyReportApi,
			VerifyRepairService iVerifyRepairApi,
			VerifyTaskService iTbVerifyTaskApi) {
		super();
		this.theadName = theadName;
		this.tbVerifyRepair = tbVerifyRepair;
		this.iVerifyReportApi = iVerifyReportApi;
		this.iVerifyRepairApi = iVerifyRepairApi;
		this.iTbVerifyTaskApi = iTbVerifyTaskApi;
	}



	@Override
	public void run() {
		log.info(theadName+"已启动.....");
		try{
			boolean runStatus=true;
			TbVerifyConfig tbVerifyConfig=iVerifyReportApi.findVerifyConfigByThing("",tbVerifyRepair);//更具原表信息和目标表信息查询配置信息
			while(runStatus){
				if(VerifyDataCache.VERIFY_CONFIG_CACHE!=null&&VerifyDataCache.VERIFY_CONFIG_CACHE.containsKey(tbVerifyConfig.getVerify_id())){
					tbVerifyConfig=VerifyDataCache.VERIFY_CONFIG_CACHE.get(tbVerifyConfig.getVerify_id());
				}
				List<String> incids=new ArrayList<String>();
				try{
					TbVerifyLog paramVerifyLog=new TbVerifyLog();
					paramVerifyLog.setTarget_table(tbVerifyConfig.getTarget_table());
					paramVerifyLog.setSource_table(tbVerifyConfig.getSource_table());
					paramVerifyLog.setVerify_type(tbVerifyRepair.getVerify_type());
					paramVerifyLog.setLimit(1000);
					paramVerifyLog.setRepair_status(1);
					paramVerifyLog.setStart_time(tbVerifyRepair.getStart_time());
					paramVerifyLog.setEnd_time(tbVerifyRepair.getEnd_time());
					List<TbVerifyRepair> repairList=iVerifyRepairApi.findVerifyRepairLimit("", paramVerifyLog);
					if(repairList!=null&&repairList.size()>0){
						for(TbVerifyRepair item:repairList){
							incids.add(item.getSiebel_row_id());
						}
						Map<String,Object> paramMap=new HashMap<String,Object>();
						paramMap.put("source_inc_table",tbVerifyConfig.getSource_inc_table());
						paramMap.put("incids", incids);
						paramMap.put("start_time", paramVerifyLog.getStart_time());
						paramMap.put("end_time", paramVerifyLog.getEnd_time());
						iVerifyRepairApi.updateSiebelIncByTableRowIds(tbVerifyConfig.getSource_base_code(),paramMap);//更新待稽核的数据
					}else{
						runStatus=false;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(incids!=null&&incids.size()>0){
					Map<String,Object> paramLogMap=new HashMap<String,Object>();
					paramLogMap.put("source_table",tbVerifyConfig.getSource_table());
					paramLogMap.put("target_table", tbVerifyConfig.getTarget_table());
					paramLogMap.put("verify_type", tbVerifyRepair.getVerify_type());
					paramLogMap.put("incids", incids);
					paramLogMap.put("start_time", tbVerifyRepair.getStart_time());
					paramLogMap.put("end_time", tbVerifyRepair.getEnd_time());
					iVerifyRepairApi.deleteBatchVerifyLog("", paramLogMap);
				}
			}
		}catch(Exception e){
			log.info(theadName+"再次稽核更新异常...."+e.getMessage(),e);
		}
		log.info(theadName+"再次稽核更新完成！");
		
	}

}
