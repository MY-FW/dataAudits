package org.datagroup.dataAudit.thread;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.service.IVerifyRepairApi;
import org.datagroup.dataAudit.service.IVerifyReportApi;
import org.datagroup.dataAudit.service.impl.VerifyRepairService;

/**
 * 【稽核数据恢复】
 * @author yannannan
 * @createTime 2018年12月7日
 * @updateTime 2018年12月7日
 * @desc
 */
public class VerifyRepairThread extends Thread{  
	private static final Logger log = LogManager.getLogger(VerifyRepairThread.class);
	private String theadName;
	private TbVerifyLog tbVerifyLog;
	private VerifyRepairService iVerifyRepairApi;

	public VerifyRepairThread(String theadName, TbVerifyLog tbVerifyLog,
			VerifyRepairService iVerifyRepairApi) { 
		super();
		this.theadName = theadName;
		this.tbVerifyLog = tbVerifyLog;
		this.iVerifyRepairApi = iVerifyRepairApi;
	}


	@Override
	public void run() {
			log.info(theadName+"修复推送线程启动....");
		try{
			boolean runStatus=true;
			long pushCount=0;
			TbVerifyRepairTask tbVerifyRepairTask=new TbVerifyRepairTask();
				tbVerifyRepairTask.setPush_status("TSZ");//TSZ推送中、TSF推送完成、TSE推送异常
				tbVerifyRepairTask.setRepair_start_time(DateUtil.format(new Date(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss"));
				tbVerifyRepairTask.setSource_table(tbVerifyLog.getSource_table());
				tbVerifyRepairTask.setTarget_table(tbVerifyLog.getTarget_table());
				tbVerifyRepairTask.setThread_name(theadName);
				tbVerifyRepairTask.setVerify_type(tbVerifyLog.getVerify_type());
				tbVerifyRepairTask.setVerify_desc(tbVerifyLog.getVerify_desc());
				tbVerifyLog.setLimit(1000);
				tbVerifyLog.setRepair_status(0);//未修复的数据
				boolean succStatus=false;//true程序状态、false用户终止任务
				boolean createStatus=false;
				Map<String,Object> paramMap=new HashMap<String,Object>();
				paramMap.put("source_table", tbVerifyLog.getSource_table());
				paramMap.put("target_table", tbVerifyLog.getTarget_table());
				paramMap.put("verify_type", tbVerifyLog.getVerify_type());
			while(runStatus){
				long startTime=System.currentTimeMillis();
				boolean errorStatus=false;
				int repairCount=0;
				try{
					if(createStatus==false){
						iVerifyRepairApi.insertOrUpdateRepairTask("", tbVerifyRepairTask);
						createStatus=true;
					}
					List<String> rowids=new ArrayList<String>();
					List<TbVerifyRepair> tbVerifyRepairList=iVerifyRepairApi.findVerifyRepairLimit("", tbVerifyLog);
					if(tbVerifyRepairList!=null&&tbVerifyRepairList.size()>0){
						repairCount=tbVerifyRepairList.size();
						pushCount+=tbVerifyRepairList.size();
						iVerifyRepairApi.insertVerifyRepair("", tbVerifyRepairList);
					}else{
						succStatus=true;//推送完成
						runStatus=false;
						break;
					}
					for(TbVerifyRepair item:tbVerifyRepairList){
						rowids.add(item.getSiebel_row_id());
					}
					paramMap.put("rowids", rowids);
					paramMap.put("repair_status",1);//已恢复状态
					paramMap.put("start_time",tbVerifyLog.getStart_time());
					paramMap.put("end_time",tbVerifyLog.getEnd_time());
				}catch(Exception e){
					log.info(theadName+"数据恢复推送异常!"+e.getMessage(),e);
					tbVerifyRepairTask.setPush_status("TSE");
					if(e.getMessage().length()>5000){
						tbVerifyRepairTask.setPush_error_msg(e.getMessage().substring(0,5000));
					}else{
						tbVerifyRepairTask.setPush_error_msg(e.getMessage());
					}
					errorStatus=true;
				}
				if(errorStatus==false&&repairCount>0){//无错误才更新
				//插入成功在开始更新状态
					iVerifyRepairApi.updateVerifyLogRepairStatus("", paramMap);
					long endTime=System.currentTimeMillis();
					log.info(theadName+"repair success...."+(endTime-startTime));
				}
			}
			if(succStatus==true){
				tbVerifyRepairTask.setRepair_end_time(DateUtil.format(new Date(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss"));
				tbVerifyRepairTask.setPush_status("TSF");//推送完成
				tbVerifyRepairTask.setPush_count(pushCount);
			}
			iVerifyRepairApi.insertOrUpdateRepairTask("", tbVerifyRepairTask);
			log.info(theadName+"恢复线程执行完成!");
		}catch(Exception e){
			log.info(theadName+"线程启动异常......."+e.getMessage(),e);
			e.printStackTrace();
		}
		
	}
		
}
