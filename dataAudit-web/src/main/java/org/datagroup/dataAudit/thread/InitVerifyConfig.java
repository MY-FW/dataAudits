package org.datagroup.dataAudit.thread;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.util.VerifyDataCache;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;

public class InitVerifyConfig extends Thread{
	private static final Logger log = LogManager.getLogger(InitVerifyConfig.class);
	private VerifyTaskService iVerifyTaskApi;
	
	public InitVerifyConfig(VerifyTaskService iVerifyTaskApi) {
		super();
		this.iVerifyTaskApi = iVerifyTaskApi;
	}

	@Override
	public void run() {
		try{
			while(true){
				List<TbVerifyConfig> verifyConfigList=iVerifyTaskApi.findVerifyConfigCache("");
				for(TbVerifyConfig item:verifyConfigList){
					VerifyDataCache.VERIFY_CONFIG_CACHE.put(item.getVerify_id(), item);
				}
				Thread.sleep(1000L*30);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
}
