package org.datagroup.dataAudit.thread;

/**
 * 【稽核任务功能执行线程，一个表一个线程】
 */
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.common.util.VerifyDataCache;
import org.datagroup.dataAudit.common.util.VerifyDataModule;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;

/**
 * 
 * @author yannannan
 * @createTime 2018年11月30日
 * @updateTime 2018年11月30日
 * @desc 【稽核任务调度处理】
 */
public class VerifyTaskThread extends Thread{ 
	private static final Logger log = LogManager.getLogger(VerifyTaskThread.class);
	private String threadName="";
	private TbVerifyConfig verifyConfig;
	private VerifyTaskService iTbVerifyTaskApi;

	public VerifyTaskThread(String threadName, TbVerifyConfig verifyConfig,
			VerifyTaskService iTbVerifyTaskApi) {
		super();
		this.threadName = threadName;
		this.verifyConfig = verifyConfig;
		this.iTbVerifyTaskApi = iTbVerifyTaskApi;
	}





	@Override
	public void run() { 
	   
		try{
			if(VerifyDataCache.THREAD_STATUS_CACHE.containsKey(threadName)){
				TbVerifyTask tbVerifyTask=new TbVerifyTask();
					tbVerifyTask.setThread_name(threadName);
				   if(verifyConfig!=null){
						 tbVerifyTask.setVerify_id(verifyConfig.getVerify_id());
						 List<Integer> verifyIds=new ArrayList<Integer>();
						 verifyIds.add(verifyConfig.getVerify_id());
						boolean succStatus=false;//true程序状态、false用户终止任务
						boolean createStatus=false;
						long verifyCount=0;
						while(VerifyDataCache.THREAD_STATUS_CACHE.get(threadName)){//判断缓存中数据如果是启动状态则一直执行稽核任务
							if(VerifyDataCache.VERIFY_CONFIG_CACHE.containsKey(verifyConfig.getVerify_id())){
								verifyConfig=VerifyDataCache.VERIFY_CONFIG_CACHE.get(verifyConfig.getVerify_id());
							}
							try{
								if(createStatus==false){
									tbVerifyTask.setTask_status("70B");//执行中
									iTbVerifyTaskApi.insertOrUpdateTaskStatus("",tbVerifyTask);//添加任务记录
									createStatus=true;//下次不在添加
								}
								long startTime=System.currentTimeMillis();
								//查询到sibele增量信息
								List<SiebelIncPojo> siebelIncList=iTbVerifyTaskApi.findSiebelIncList(verifyConfig.getSource_base_code(),verifyConfig);
								List<TbVerifyLog> verifyLogList=new ArrayList<TbVerifyLog>();
								VerifyDataModule verifyModule=new VerifyDataModule();
								if(siebelIncList!=null&&siebelIncList.size()>0){
									verifyCount+=siebelIncList.size();
									List<String> rowids=new ArrayList<String>();
									List<String> incids=new ArrayList<String>();
									for(SiebelIncPojo siebelInc:siebelIncList){
										if(siebelInc.getRow_id()==null||siebelInc.getRow_id().equals("")||siebelInc.getTable_row_id()==null||siebelInc.getTable_row_id().equals("")){
											log.info("增量inc表异常：【"+siebelInc.getRow_id()+"#"+siebelInc.getTable_row_id()+"】");
											continue;
										}
										rowids.add(siebelInc.getRow_id());
										incids.add(siebelInc.getTable_row_id());
										Map<String,Object> targetParamMap=new HashMap<String,Object>();
												targetParamMap.put("target_table",verifyConfig.getTarget_table().trim());
												targetParamMap.put("table_row_id", siebelInc.getTable_row_id());
										if(siebelInc.getOp_type().trim().toUpperCase().contains("DELETE")){
											int targetCount=iTbVerifyTaskApi.findTargetVerifyDeleteCount(verifyConfig.getTarget_base_code(), targetParamMap);
											verifyModule.validateDeleteExist(verifyConfig, siebelInc, targetCount, verifyLogList);
										}else{
											Map<String,Object> functionParam=new HashMap<String,Object>();
													StringBuilder builderProc=new StringBuilder();
													builderProc.append(verifyConfig.getProcess_name().trim()+"(");
													builderProc.append("'"+siebelInc.getTable_row_id()+"'");
													builderProc.append(")");
													functionParam.put("process_name",builderProc.toString());
													functionParam.put("table_row_id", siebelInc.getTable_row_id());
											String callStr=iTbVerifyTaskApi.callFunction(verifyConfig.getSource_base_code(),functionParam);
											targetParamMap.put("target_fields",verifyConfig.getTarget_fields().trim().toUpperCase());
												if(callStr!=null&&!callStr.equals("")){
													String[] sk_fieldStr=callStr.split(";")[0].split("#");
													if(sk_fieldStr.length>1){
														sk_fieldStr=sk_fieldStr[1].split("=");
													  targetParamMap.put("target_sk_field", sk_fieldStr[0].toUpperCase().trim());//分片建查询目标数据
													  targetParamMap.put("target_sk_value", sk_fieldStr[1].trim());
													}else{
														targetParamMap.put("target_sk_field",null);//分片建查询目标数据
														targetParamMap.put("target_sk_value",null);
													}
												}
											List<Map<String,Object>> targetValueList=iTbVerifyTaskApi.findVerifyDataList(verifyConfig.getTarget_base_code(), targetParamMap);
												if(targetValueList==null||targetValueList.size()==0){
													targetParamMap.put("target_sk_field",null);//分片建查询目标数据
													targetParamMap.put("target_sk_value",null);
													targetValueList=iTbVerifyTaskApi.findVerifyDataList(verifyConfig.getTarget_base_code(), targetParamMap);
												}
											
											
											TbVerifyLog verifyLog=verifyModule.validateCallExist(verifyConfig,callStr,targetValueList,siebelInc);
											if(verifyLog!=null){
												//数据不存在情况直接跳过后续操作
												verifyLogList.add(verifyLog);
												continue;
											}
											String[] callStrArray=callStr.trim().split(";");//得到存储过程返回的条目源数据
											for(String srcValueStr:callStrArray){
												String[] srcItem=srcValueStr.trim().split("=");//得到其中一个key,value
												if(srcItem[0].indexOf("@")!=-1&&srcItem[0].lastIndexOf("@")!=-1){//特殊
													if(srcItem[1].split("@").length<=1){
														continue;
													}
												}
												verifyModule.validateSrcFieldValueExist(verifyConfig, siebelInc, srcItem, targetValueList, verifyLogList);
											}
										
										}
									}
									
									long endTime=System.currentTimeMillis();
									log.info("【"+verifyConfig.getSource_table()+"】,【"+verifyConfig.getTarget_table()+"】一批稽核耗时："+(endTime-startTime));
									iTbVerifyTaskApi.insertBatchVerifyLog("",verifyLogList);
									Map<String,Object> paramSiebelMap=new HashMap<String,Object>();
									paramSiebelMap.put("source_inc_table",verifyConfig.getSource_inc_table());
									paramSiebelMap.put("rowids",rowids);
									paramSiebelMap.put("incids",incids);
									paramSiebelMap.put("status_date",DateUtil.format(new Date(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss"));
									paramSiebelMap.put("verify_start_time",verifyConfig.getVerify_start_time());
									paramSiebelMap.put("verify_end_time", verifyConfig.getVerify_end_time());
									iTbVerifyTaskApi.updateSiebelIncStatus(verifyConfig.getSource_base_code(),paramSiebelMap);
									siebelIncList.clear();
									verifyLogList.clear();
									
								}else{
									tbVerifyTask.setThread_name(threadName);
									tbVerifyTask.setTask_status("70C");//设置状态执行完成
									tbVerifyTask.setTask_end_time(DateUtil.format(new Date(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss"));
									tbVerifyTask.setVerify_count(verifyCount);
										log.info(threadName+"执行完成，暂无数据稽核!休息60秒!");
									   succStatus=false;//true程序状态、false用户终止任务
									   iTbVerifyTaskApi.insertOrUpdateTaskStatus("", tbVerifyTask);
									 Thread.sleep(1000L*60);
								}
							}catch(Exception e){
								tbVerifyTask.setTask_status("70E");//设置状态执行完成
								if(e.getMessage().length()>5000){
									tbVerifyTask.setTask_error_msg(e.getMessage().substring(0,5000));
								}else{
									tbVerifyTask.setTask_error_msg(e.getMessage());
								}
								 iTbVerifyTaskApi.insertOrUpdateTaskStatus("", tbVerifyTask);
								log.info(threadName+"执行异常！"+e.getMessage(),e);
								Thread.sleep(1000L*60);
							}
						}
						
						
						if(succStatus==false){//如果是用户操作当前状态为停止，则设置状态为待执行//true程序状态、false用户终止任务
							log.info(threadName+"被设置待启动！");
							tbVerifyTask.setTask_status("70A");
							VerifyDataCache.THREAD_STATUS_CACHE.put(threadName, false);//设置状态为待执行false
						}
						iTbVerifyTaskApi.insertOrUpdateTaskStatus("", tbVerifyTask);
					}else{
						VerifyDataCache.THREAD_STATUS_CACHE.put(threadName, false);//设置状态为待执行false
							tbVerifyTask.setTask_status("70E");//设置状态执行完成
							tbVerifyTask.setTask_error_msg("启动失败！VerifyConfig参数异常！");
							iTbVerifyTaskApi.insertOrUpdateTaskStatus("TYONG",tbVerifyTask);
						log.info(threadName+"启动失败！VerifyConfig参数异常！");
					}
			}else{
				log.info(threadName+"启动失败：THREAD_STATUS_CACHE任务状态缓存不存在!");
			}
		}catch(Exception e){
			VerifyDataCache.THREAD_STATUS_CACHE.put(threadName, false);//设置状态为待执行falses
			log.info(threadName+"任务启动异常"+e.getMessage(),e);
			e.printStackTrace();
		}
	}
	
	
	
}
