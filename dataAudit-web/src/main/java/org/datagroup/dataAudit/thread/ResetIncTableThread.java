package org.datagroup.dataAudit.thread;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;

/**
 * 【重置初始化inc增量表70C状态，为重新稽核做准备】
 * @author yannannan
 * @createTime 2018年12月24日
 * @updateTime 2018年12月24日
 * @desc
 */
public class ResetIncTableThread extends Thread{ 
	private static final Logger log = LogManager.getLogger(ResetIncTableThread.class);
	private VerifyTaskService iTbVerifyTaskApi;
	private TbVerifyConfig tbVerifyConfig;
	
	public ResetIncTableThread(VerifyTaskService iTbVerifyTaskApi,
			TbVerifyConfig tbVerifyConfig) {
		super();
		this.iTbVerifyTaskApi = iTbVerifyTaskApi;
		this.tbVerifyConfig = tbVerifyConfig;
	}

	@Override
	public void run() {
		try{
			if(tbVerifyConfig!=null){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				tbVerifyConfig.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
				iTbVerifyTaskApi.updateResetIncStatus(tbVerifyConfig.getSource_base_code(), tbVerifyConfig);
				log.info("【"+tbVerifyConfig.getSource_inc_table()+"】重置完成！");
			}
		}catch(Exception e){
			log.info("【"+tbVerifyConfig.getSource_inc_table()+"】重置异常!"+e.getMessage(),e);
		}
	}
	
}
