package org.datagroup.dataAudit.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.common.util.VerifyDataCache;
import org.datagroup.dataAudit.service.impl.VerifyTaskService;

/**
 * 【稽核功能任务初始化执行】
 * @author yannannan
 * @createTime 2018年12月7日
 * @updateTime 2018年12月7日
 * @desc
 */
public class initTaskThread extends Thread{ 
	private static final Logger log = LogManager.getLogger(initTaskThread.class);
	private VerifyTaskService iTbVerifyTaskApi;
	private List<TbVerifyConfig> verifyConfigList;
	
	public initTaskThread(VerifyTaskService iTbVerifyTaskApi,
			List<TbVerifyConfig> verifyConfigList) {
		super();
		this.iTbVerifyTaskApi = iTbVerifyTaskApi;
		this.verifyConfigList = verifyConfigList;
	}

	@Override
	public void run() {  
	try{
			log.info("初始化启动稽核任务！");
			if(verifyConfigList==null||verifyConfigList.size()==0){
				verifyConfigList=iTbVerifyTaskApi.initTaskVerifyConfig("");//WHERE STATUS='KY' AND AUDIT_STATUS='70B'
			}
			if(verifyConfigList!=null&&verifyConfigList.size()>0){
				ExecutorService exe=Executors.newFixedThreadPool(verifyConfigList.size());//线程池
				List<Integer> verifyIds=new ArrayList<Integer>();
				for(TbVerifyConfig item:verifyConfigList){
					DataColumnType paramColumnType=new DataColumnType();
					 	paramColumnType.setTable_name(item.getTarget_table());
					 	paramColumnType.setTable_schema(item.getTarget_base_name());
					List<DataColumnType> dataqColumnType=iTbVerifyTaskApi.findTableColumnDataType(item.getTarget_base_code(),paramColumnType);
					for(DataColumnType dataTypeItem:dataqColumnType){
						VerifyDataCache.putColumnType(dataTypeItem);
					}
					String threadName="task"+item.getVerify_id();
					VerifyDataCache.THREAD_STATUS_CACHE.put(threadName,true);//设置启动状态true
					verifyIds.add(item.getVerify_id());
					exe.execute(new VerifyTaskThread(threadName,item,iTbVerifyTaskApi));
				 }
				iTbVerifyTaskApi.updateVerifyConfigRunStatus("", verifyIds);//批量更新稽核运行状态
			}
		}catch(Exception e){
			log.info("初始化稽核线程异常"+e.getMessage(),e);
			e.printStackTrace();
		}
	}
	
	
	
	
	
}
