package org.datagroup.dataAudit.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.basepo.ResponseInfo;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;
import org.datagroup.dataAudit.service.ISysDataSourceApi;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;

@Component("sysDataSourceApi")
public class SysDataSourceService {
  private static final Logger logger = LogManager.getLogger(SysDataSourceService.class);

  @Reference(retries = 1, timeout = 100000)
  private ISysDataSourceApi sysDataSourceApi;

  public ResponseInfo<List<SysDataSourceInfo>> query(String searchParam) {
    return sysDataSourceApi.query(searchParam);
  }

  public ResponseInfo addDs(SysDataSourceInfo sysDataSource) {
    return sysDataSourceApi.addDs(sysDataSource);
  }

  public ResponseInfo editDs(SysDataSourceInfo sysDataSource) {
    return sysDataSourceApi.editDs(sysDataSource);
  }

  public ResponseInfo delDs(String baseCode) {
    return sysDataSourceApi.delDs(baseCode);
  }

  public ResponseInfo createDs(String baseCode, String baseStatus) {
    return sysDataSourceApi.createDs(baseCode, baseStatus);
  }

}
