package org.datagroup.dataAudit.service;

import java.util.List;

import org.datagroup.dataAudit.common.po.TbVerifyConfig;

public interface IVerifyConfigApi {


	// /**
	// * 根据SEIBEL表和新表表名查询配置项
	// *
	// * @param sourceTable
	// * @param targetTable
	// * @return
	// */
	// public List<TbVerifyConfig> findByParam(String sourceTable, String
	// targetTable);
/*	public int findVerifyConfigCount(TbVerifyConfig tbVerifyConfig);*/

	public List<TbVerifyConfig> findVerifyConfigPageAll(String tbVerifyConfig);

	public TbVerifyConfig getTbVerifyConfigList(String verify_id);

	public int updateTbVerifyConfig(TbVerifyConfig tbVerifyConfig);

	public int delete(String id);
	
	public int TbVerifyConfigadd(TbVerifyConfig tbVerifyConfig);
}
