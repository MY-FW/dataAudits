package org.datagroup.dataAudit.service.impl;

import java.util.List;

import org.datagroup.dataAudit.common.po.TbVerifyTask;
import org.datagroup.dataAudit.common.util.Page;
import org.datagroup.dataAudit.service.ITbVerifyTaskApi;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.Map;

import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;

@Component("iTbVerifyTaskApi")
public class VerifyTaskService {

	 @Reference(retries =0, timeout =1000000)
	 private ITbVerifyTaskApi iTbVerifyTaskApi;
	 
	 
	 /**
	  * 【查询稽核任务task信息到界面】
	 */
	public JSONObject findVerifyTaskPageAll(String dataSourceCode,TbVerifyTask tbVerifyTask){
			int num=iTbVerifyTaskApi.findVerifyTaskCount(dataSourceCode,tbVerifyTask);
			JSONObject jsonObject = new JSONObject();
				jsonObject.put("currentPageIndex",1);
				jsonObject.put("pageSize",tbVerifyTask.getPageSize());
		        jsonObject.put("code", 0);
		        jsonObject.put("msg", "查询成功!");
		        jsonObject.put("count",num);
		        List<TbVerifyTask> verifyTaskList=iTbVerifyTaskApi.findVerifyTaskPageAll(dataSourceCode,tbVerifyTask);
		     jsonObject.put("data", verifyTaskList);
			return jsonObject;
	}
		
		
	 
	
	/*******************【稽核业务】*************************/
	
	/**
	 * 【初始化查询所有待启动稽核并开始任务】
	 * 【当程序重新启动时加载稽核配置信息并启动稽核】
	 */
	public List<TbVerifyConfig> initTaskVerifyConfig(String dataSourceCode){
		return iTbVerifyTaskApi.initTaskVerifyConfig(dataSourceCode);
	}
	
	
	/**
	 * 【查询数据类型到数据缓存】
	 */
	public List<DataColumnType> findTableColumnDataType(String dataSourceCode,DataColumnType dataColumnType){
		return iTbVerifyTaskApi.findTableColumnDataType(dataSourceCode, dataColumnType);
	}
	
	
	/**
	 * 【查询增量inc表信息】【不分页为稽核初始化准备】
	 */
	public List<SiebelIncPojo> findSiebelIncList(String dataSourceCode,TbVerifyConfig tbVerifyConfig){
		return iTbVerifyTaskApi.findSiebelIncList(dataSourceCode,tbVerifyConfig);
	}
	
	
	/**
	 * 【修改任务状态单挑 upate Task】
	 */
	public void insertOrUpdateTaskStatus(String dataSourceCode,TbVerifyTask tbVerifyTask){
		TbVerifyTask oldTask=iTbVerifyTaskApi.findTaskByVerifyId(dataSourceCode, tbVerifyTask);
		if(oldTask!=null){
			oldTask.setTask_end_time(tbVerifyTask.getTask_end_time());
			oldTask.setTask_status(tbVerifyTask.getTask_status());
			oldTask.setTask_error_msg(tbVerifyTask.getTask_error_msg());
			oldTask.setTask_start_time(tbVerifyTask.getTask_start_time());
			oldTask.setVerify_count(tbVerifyTask.getVerify_count());
			iTbVerifyTaskApi.updateVerifyTask(dataSourceCode, oldTask);
		}else{
			String startTime=DateUtil.format(new Date(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss");
			tbVerifyTask.setTask_start_time(startTime);
			tbVerifyTask.setCreate_time(startTime);
			iTbVerifyTaskApi.insertVerifyTask(dataSourceCode, tbVerifyTask);
		}
	}
	
	
	
	/**
	 * 【查询稽核目标未知表总量】
	 */
	public int findTargetVerifyDeleteCount(String dataSourceCode,Map<String,Object> paramMap){
		return iTbVerifyTaskApi.findTargetVerifyDeleteCount(dataSourceCode,paramMap);
	}
	
	
	/**
	 * 【调用存储过程】
	 */
	public String callFunction(String dataSourceCode,Map<String,Object> paramMap){
		return iTbVerifyTaskApi.callFunction(dataSourceCode, paramMap);
	}
	
	
	/**
	 * 【查询目标表数据结果】
	 */
	public List<Map<String,Object>> findVerifyDataList(String dataSourceCode,Map<String,Object> paramMap){
		return iTbVerifyTaskApi.findVerifyDataList(dataSourceCode,paramMap);
	}
	
	/**
	 * 【稽核日志插入】
	 */
	public void insertBatchVerifyLog(String dataSourceCode,List<TbVerifyLog> tbVerifyLogList){
		iTbVerifyTaskApi.insertBatchVerifyLog(dataSourceCode, tbVerifyLogList);
	}
	
	
	/**
	 * 【批量更改增量inc 的状态】
	 */
	public void updateSiebelIncStatus(String dataSourceCode,Map<String,Object> paramMap){
		iTbVerifyTaskApi.updateSiebelIncStatus(dataSourceCode,paramMap);
	}
	
	

	/**
	 * 【查询配置信息单条】
	 */
	public List<TbVerifyConfig> findVerifyConfgByIDs(String dataSourceCode,List<Integer> verifyIds){
		return iTbVerifyTaskApi.findVerifyConfgByIDs(dataSourceCode, verifyIds);
	}
	
	
	/**
	 *【批量更改稽核信息状态】
	 */
	public void updateVerifyConfigRunStatus(String dataSourceCode,List<Integer> verifyIds){
		 iTbVerifyTaskApi.updateVerifyConfigRunStatus(dataSourceCode, verifyIds);
	}
	
	/**
	 * 【初始化配置信息】
	 */
	public List<TbVerifyConfig> findVerifyConfigCache(String dataSourceCode){
		return iTbVerifyTaskApi.findVerifyConfigCache(dataSourceCode);
	}
	
	
	/**
	 * 【重置inc增量表状态信息】
	 */
	public void updateResetIncStatus(String dataSourceCode,TbVerifyConfig tbVerifyConfig){
		 iTbVerifyTaskApi.updateResetIncStatus(dataSourceCode,tbVerifyConfig);
	}
	
	
}
