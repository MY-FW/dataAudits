package org.datagroup.dataAudit.service;

import java.util.List;

import org.datagroup.dataAudit.common.basepo.ResponseInfo;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;

public interface ISysDataSourceApi {

  /**
   * @Description:查询数据源信息
   * @param dataSourceCode
   * @return
   * @return List<TbSysDataSource>
   * @author Chentw
   * @date 2018年11月29日
   */
  public ResponseInfo<List<SysDataSourceInfo>> query(String searchParam);

  /**
   * @Description:添加数据源信息、并创建数据源
   * @param tbSysDataSource
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年11月26日
   */
  public ResponseInfo addDs(SysDataSourceInfo sysDataSource);

  /**
   * @Description:根据数据源编码删除数据源信息
   * @param dataSourceCode
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年11月29日
   */
  public ResponseInfo delDs(String baseCode);

  /**
   * @Description:修改配置源信息
   * @param tbSysdataSource
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年11月29日
   */
  public ResponseInfo editDs(SysDataSourceInfo sysdataSource);

  /**
   * @Description:创建数据源
   * @param dataSourceCode
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年12月3日
   */
  public ResponseInfo createDs(String baseCode, String baseStatus);

}
