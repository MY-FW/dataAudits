package org.datagroup.dataAudit.service;

import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;

public interface IVerifyReportApi {

  public List<TbVerifyLog> findLogGroupBy(String dataSourceCode,TbVerifyLog tbVerifyLog);
  public int findLogGroupByCount(String dataSourceCode,TbVerifyLog tbVerifyLog);
  public int findDetailCount(String dataSourceCode,TbVerifyLog tbVerifyLog);
  public List<TbVerifyLog> findDetail(String dataSourceCode,TbVerifyLog tbVerifyLog);
  public List<TbVerifyLog> findVerifyLogByRepair(String dataSourceCode,List<TbVerifyLog> tbVerifyLogs);
  public void updateTbVerifyLog(String dataSourceCode,List<TbVerifyRepair> tbVerifyRepairs);
  public int findRepairTaskCount(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask);
  public List<TbVerifyRepairTask> findRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask);

	
	/**
	 * 【根据原表信息和目标表信息查询配置信息】
	 */
	public TbVerifyConfig findVerifyConfigByThing(String dataSourceCode,TbVerifyRepair tbVerifyRepair);
  
 }
