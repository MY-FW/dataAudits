package org.datagroup.dataAudit.service.impl;

import org.datagroup.dataAudit.common.po.SysUserInfo;
import org.datagroup.dataAudit.service.ISysUserInfoApi;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;

@Component("sysUserInfoApi")
public class SysUserInfoService {

  @Reference(retries = 1, timeout = 10000)
  private ISysUserInfoApi sysUserInfoApi;
  
  public SysUserInfo findByParam(String account, String password, String userName) {
    // TODO Auto-generated method stub
    return sysUserInfoApi.findByParam(account, password, userName);
  }

}
