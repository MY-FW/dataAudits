package org.datagroup.dataAudit.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.common.util.DateUtil;
import org.datagroup.dataAudit.service.IVerifyRepairApi;
import org.datagroup.dataAudit.service.IVerifyReportApi;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Component("iVerifyRepairApi")
public class VerifyRepairService {

	 @Reference(retries = 0, timeout = 100000)
	 private IVerifyRepairApi iVerifyRepairApi;
	 
	 public void insertVerifyRepair(String dataSourceCode,List<TbVerifyRepair> repairList){
		 iVerifyRepairApi.insertVerifyRepair(dataSourceCode, repairList);
	 }
	 
	 /**
	  * 【恢复进程记录】
	  * @param dataSourceCode
	  * @param tbVerifyRepairTask
	  */
	 public void insertOrUpdateRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask){
		 TbVerifyRepairTask oldRepairTask=iVerifyRepairApi.findVerifyRepairTaskById(dataSourceCode, tbVerifyRepairTask);
		 	if(oldRepairTask!=null){
		 		oldRepairTask.setRepair_end_time(tbVerifyRepairTask.getRepair_end_time());
		 		oldRepairTask.setPush_error_msg(tbVerifyRepairTask.getPush_error_msg());
		 		oldRepairTask.setPush_status(tbVerifyRepairTask.getPush_status());
		 		oldRepairTask.setPush_count(tbVerifyRepairTask.getPush_count());
		 		oldRepairTask.setRepair_start_time(tbVerifyRepairTask.getRepair_start_time());
		 		iVerifyRepairApi.updateVerifyRepairTask(dataSourceCode, oldRepairTask);
		 	}else{
		 		String startTime=DateUtil.format(new Date(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss");
			 		tbVerifyRepairTask.setRepair_start_time(startTime);
			 		tbVerifyRepairTask.setPush_error_msg(tbVerifyRepairTask.getPush_error_msg());
			 		tbVerifyRepairTask.setPush_status(tbVerifyRepairTask.getPush_status());
			 		tbVerifyRepairTask.setPush_count(tbVerifyRepairTask.getPush_count());
		 		iVerifyRepairApi.insertVerifyRepairTask(dataSourceCode, tbVerifyRepairTask);
		 	}
		 
	 }
	 
	 
	 
	 /**
	  * 【查询待修复数据】
	  */
	public List<TbVerifyRepair> findVerifyRepairLimit(String dataSourceCode,TbVerifyLog tbVerifyLog){
		return iVerifyRepairApi.findVerifyRepairLimit(dataSourceCode, tbVerifyLog);
	}
	
	
	
	/**
	 * 【批量修改稽核记录恢复状态】
	 */
	public void updateVerifyLogRepairStatus(String dataSourceCode,Map<String,Object> paramMap){
		 iVerifyRepairApi.updateVerifyLogRepairStatus(dataSourceCode, paramMap);
	}
	
	
	/**
	 * 【批量插入恢复记录表】
	 */
	public void insertBatchRepairTask(List<TbVerifyRepairTask> tbVerifyRepairTaskList){
		iVerifyRepairApi.insertBatchRepairTask(tbVerifyRepairTaskList);
	}
	 
	/**
	 * 【修复更具inc表table_row_id】
	 * @param tableRowids
	 */
	public void updateSiebelIncByTableRowIds(String dataSourceCode,Map<String,Object> paramMap){
		iVerifyRepairApi.updateSiebelIncByTableRowIds(dataSourceCode,paramMap);
	}
	
	
	/**
	 * 【delete verifyLog】
	 */
	public void deleteBatchVerifyLog(String dataSourceCode,Map<String,Object> paramMap){
		iVerifyRepairApi.deleteBatchVerifyLog(dataSourceCode,paramMap);
	}
	
	
	/**
	 * 【批量更改重置推送状态】
	 */
	public void updateRepairStatusByVerifyLog(String dataSourceCode,TbVerifyLog tbVerifyLog){
		iVerifyRepairApi.updateRepairStatusByVerifyLog(dataSourceCode,tbVerifyLog);
	}
	
	
	/**
	 * 【删除修复记录表信息】
	 */
	public void deleteTbRepairData(String dataSourceCode,TbVerifyLog tbVerifyLog){
		iVerifyRepairApi.deleteTbRepairData(dataSourceCode,tbVerifyLog);
	}
	
	
	/**
	 * 【删除repairTask数据】
	 */
	public void deleteVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask){
		iVerifyRepairApi.deleteVerifyRepairTask(dataSourceCode,tbVerifyRepairTask);
	}
	
	
	 /**
	   * 【查询修复数据记录】
	   */
	  public List<TbVerifyRepair> findVerifyRepair(String dataSourceCode,TbVerifyRepair tbVerifyRepair){
		 return iVerifyRepairApi.findVerifyRepair(dataSourceCode, tbVerifyRepair);
	  }
	  /**
		* 【查询所有修复数据数量】
	  */
	  public int findVerifyRepairCount(String dataSourceCode,TbVerifyRepair tbVerifyRepair){
		  return iVerifyRepairApi.findVerifyRepairCount(dataSourceCode,tbVerifyRepair);
	  }
	
}
