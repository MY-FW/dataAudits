package org.datagroup.dataAudit.service.impl;

import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.service.IVerifyReportApi;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;

@Component("iVerifyReportApi")
public class VerifyReportService {

	 @Reference(retries = 0, timeout = 1000000)
	 private IVerifyReportApi iVerifyReportApi;
	 
	 
	 /**
	  * 日志统计
	  * @param tbVerifyLog
	  * @param dataSourceCode
	  * @return
	  */
	public JSONObject findLogGroupBy(String dataSourceCode,TbVerifyLog tbVerifyLog){
			int num=iVerifyReportApi.findLogGroupByCount(dataSourceCode,tbVerifyLog);
			JSONObject jsonObject = new JSONObject();
		        jsonObject.put("code", 0);
		        jsonObject.put("msg", "查询成功!");
		        jsonObject.put("count",num);
		        List<TbVerifyLog> verifyLogs=iVerifyReportApi.findLogGroupBy(dataSourceCode,tbVerifyLog);
		     jsonObject.put("data", verifyLogs);
			return jsonObject;
	}
	/**
	 * 查询日志详情
	 * @param tbVerifyLog
	 * @param dataSourceCode
	 * @return
	 */
	public JSONObject findDetail(String dataSourceCode,TbVerifyLog tbVerifyLog){
		int num=iVerifyReportApi.findDetailCount(dataSourceCode,tbVerifyLog);
		JSONObject jsonObject = new JSONObject();
			//jsonObject.put("currentPageIndex",1);
			//jsonObject.put("pageSize",tbVerifyLog.getPageSize());
	        jsonObject.put("code", 0);
	        jsonObject.put("msg", "查询成功!");
	        jsonObject.put("count",num);
	        List<TbVerifyLog> verifyLogs=iVerifyReportApi.findDetail(dataSourceCode,tbVerifyLog);
	     jsonObject.put("data", verifyLogs);
		return jsonObject;
	}
	/**
	  * 恢复日志统计
	  * @param tbVerifyLog
	  * @param dataSourceCode
	  * @return
	  */
	public JSONObject findRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask) {
		int num=iVerifyReportApi.findRepairTaskCount(dataSourceCode,tbVerifyRepairTask);
		JSONObject jsonObject = new JSONObject();
	        jsonObject.put("code", 0);
	        jsonObject.put("msg", "查询成功!");
	        jsonObject.put("count",num);
	        List<TbVerifyRepairTask> verifyLogs=iVerifyReportApi.findRepairTask(dataSourceCode,tbVerifyRepairTask);
	     jsonObject.put("data", verifyLogs);
		return jsonObject;
	}
	
	
	
	/**
	 * 【根据原表信息和目标表信息查询配置信息】
	 */
	public TbVerifyConfig findVerifyConfigByThing(String dataSourceCode,TbVerifyRepair tbVerifyRepair){
		return iVerifyReportApi.findVerifyConfigByThing(dataSourceCode,tbVerifyRepair);
	}
	
	
	
}
