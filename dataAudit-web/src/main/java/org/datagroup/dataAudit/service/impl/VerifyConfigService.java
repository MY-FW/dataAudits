package org.datagroup.dataAudit.service.impl;

import java.util.List;

import org.datagroup.dataAudit.common.po.SysDataSourceInfo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.service.IVerifyConfigApi;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;

@Component("verifyConfigApi")
public class VerifyConfigService {

	@Reference(retries = 0, timeout = 1000000)
	private IVerifyConfigApi verifyConfigApi;


	//
	// /**
	// * 根据SEIBEL表和新表表名查询配置项
	// *
	// * @param sourceTable
	// * @param targetTable
	// * @return
	// */
	// public List<TbVerifyConfig> findByParam(String sourceTable, String
	// targetTable) {
	// return verifyConfigApi.findByParam(sourceTable, targetTable);
	// }
	
	  public List<TbVerifyConfig> findVerifyConfigPageAll(String tbVerifyConfig) {
		    return verifyConfigApi.findVerifyConfigPageAll(tbVerifyConfig);
		  }
	
	/*public JSONObject findVerifyConfigPageAlls(TbVerifyConfig tbVerifyConfig, String dataSourceCode) {
		int page = verifyConfigApi.findVerifyConfigCount(tbVerifyConfig);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("currentPageIndex", 1);
		jsonObject.put("pageSize", tbVerifyConfig.getPageSize());
		jsonObject.put("code", 0);
		jsonObject.put("msg", "查询成功!");
		jsonObject.put("count", page);
		List<TbVerifyConfig> tbVerifyConfigs = verifyConfigApi.findVerifyConfigPageAll(tbVerifyConfig);
		jsonObject.put("data", tbVerifyConfigs);
		return jsonObject;

	}*/

	public TbVerifyConfig getTbVerifyConfigList(String verify_id) {
		return verifyConfigApi.getTbVerifyConfigList(verify_id);

	}

	public int updateTbVerifyConfig(TbVerifyConfig tbVerifyConfig) {
		return verifyConfigApi.updateTbVerifyConfig(tbVerifyConfig);
	}

	public int delete(String id) {
		return verifyConfigApi.delete(id);

	}
	
	public int TbVerifyConfigadd(TbVerifyConfig tbVerifyConfig){
		
		return verifyConfigApi.TbVerifyConfigadd(tbVerifyConfig);
	}
}
