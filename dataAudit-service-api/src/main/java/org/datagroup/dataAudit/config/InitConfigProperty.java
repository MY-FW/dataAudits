package org.datagroup.dataAudit.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Configuration
@ConfigurationProperties(prefix = "dataaudit.config")
@PropertySource(value = "dataaudit.properties", encoding = "UTF-8")
public class InitConfigProperty {
  private String name;
  private String baseStatus;
  private String oracleBaseDcn;
  private String orcbaseUrlBySid;
  private String orcbaseUrlBySn;
  private String mysqlBaseDcn;
  private String mysqlbaseUrl;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBaseStatus() {
    return baseStatus;
  }

  public void setBaseStatus(String baseStatus) {
    this.baseStatus = baseStatus;
  }

  public String getOracleBaseDcn() {
    return oracleBaseDcn;
  }

  public void setOracleBaseDcn(String oracleBaseDcn) {
    this.oracleBaseDcn = oracleBaseDcn;
  }

  public String getOrcbaseUrlBySid() {
    return orcbaseUrlBySid;
  }

  public void setOrcbaseUrlBySid(String orcbaseUrlBySid) {
    this.orcbaseUrlBySid = orcbaseUrlBySid;
  }

  public String getOrcbaseUrlBySn() {
    return orcbaseUrlBySn;
  }

  public void setOrcbaseUrlBySn(String orcbaseUrlBySn) {
    this.orcbaseUrlBySn = orcbaseUrlBySn;
  }

  public String getMysqlBaseDcn() {
    return mysqlBaseDcn;
  }

  public void setMysqlBaseDcn(String mysqlBaseDcn) {
    this.mysqlBaseDcn = mysqlBaseDcn;
  }

  public String getMysqlbaseUrl() {
    return mysqlbaseUrl;
  }

  public void setMysqlbaseUrl(String mysqlbaseUrl) {
    this.mysqlbaseUrl = mysqlbaseUrl;
  }

}
