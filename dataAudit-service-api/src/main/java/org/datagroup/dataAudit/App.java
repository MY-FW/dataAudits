package org.datagroup.dataAudit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.config.InitConfigProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;

/**
 * @功能说明: 启动类
 * @author Chentw
 * @versionr
 * @版权: 版权所有 (c) 2018
 * @创建日期: 2018年11月26日
 */
@EnableDubboConfiguration
@EnableCaching
@EnableConfigurationProperties(InitConfigProperty.class)
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackages = { "org.datagroup.dataAudit" })
public class App {
  private static final Logger logger = LogManager.getLogger(App.class);

  public static void main(String[] args) throws Exception {
    SpringApplication.run(App.class, args);
  }

}
