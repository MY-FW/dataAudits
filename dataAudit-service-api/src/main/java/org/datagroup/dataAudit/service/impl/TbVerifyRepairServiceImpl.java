package org.datagroup.dataAudit.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.dao.db.dynamicdb.TargetDataSource;
import org.datagroup.dataAudit.dao.mapper.TbVerifyRepairMapper;
import org.datagroup.dataAudit.service.IVerifyRepairApi;
import org.datagroup.dataAudit.service.IVerifyReportApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

@Service(interfaceClass = IVerifyRepairApi.class, retries = 0,timeout = 1000000)
@Component
public class TbVerifyRepairServiceImpl implements IVerifyRepairApi{
	private static final Logger logger = LogManager.getLogger(TbVerifyRepairServiceImpl.class);
	 @Autowired
	 private TbVerifyRepairMapper tbVerifyRepairMapper;
	
	@Override
	public void insertVerifyRepair(String dataSourceCode,
			List<TbVerifyRepair> repairList) {
		tbVerifyRepairMapper.insertVerifyRepair(repairList);
	}
	
	
	
	/**
	 * 【查询修复状态】
	 */
	public TbVerifyRepairTask findVerifyRepairTaskById(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask){
		return tbVerifyRepairMapper.findVerifyRepairTaskById(tbVerifyRepairTask);
	}
	
	
	/**
	 * 【插入修复状态记录】
	 */
	public void insertVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask){
		 tbVerifyRepairMapper.insertVerifyRepairTask(tbVerifyRepairTask);
	}
	
	
	/**
	 * 【修改修复状态记录】
	 */
	public void updateVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask){
		 tbVerifyRepairMapper.updateVerifyRepairTask(tbVerifyRepairTask);
	}
	
	
	/**
	 * 【查询待修复数据】
	 */
	public List<TbVerifyRepair> findVerifyRepairLimit(String dataSourceCode,TbVerifyLog tbVerifyLog){
		return tbVerifyRepairMapper.findVerifyRepairLimit(tbVerifyLog);
	}
	
	
	/**
	 * 【批量修改稽核记录恢复状态】
	 */
	public void updateVerifyLogRepairStatus(String dataSourceCode,Map<String,Object> paramMap){
		 tbVerifyRepairMapper.updateVerifyLogRepairStatus(paramMap);
	}
	
	
	/**
	 * 【批量插入恢复记录表】
	 */
	public void insertBatchRepairTask(List<TbVerifyRepairTask> tbVerifyRepairTaskList){
		tbVerifyRepairMapper.insertBatchRepairTask(tbVerifyRepairTaskList);
	}
	
	/**
	 * 【修复更具inc表table_row_id】
	 * @param tableRowids
	 */
	@TargetDataSource
	public void updateSiebelIncByTableRowIds(String dataSourceCode,Map<String,Object> paramMap){
		tbVerifyRepairMapper.updateSiebelIncByTableRowIds(paramMap);
	}
	
	
	/**
	 * 【delete verifyLog】
	 */
	public void deleteBatchVerifyLog(String dataSourceCode,Map<String,Object> paramMap){
		 tbVerifyRepairMapper.deleteBatchVerifyLog(paramMap);
	}
	


	/**
	 * 【批量更改重置推送状态】
	 */
	public void updateRepairStatusByVerifyLog(String dataSourceCode,TbVerifyLog tbVerifyLog){
		tbVerifyRepairMapper.updateRepairStatusByVerifyLog(tbVerifyLog);
	}
	
	/**
	 * 【删除修复记录表信息】
	 */
	public void deleteTbRepairData(String dataSourceCode,TbVerifyLog tbVerifyLog){
		tbVerifyRepairMapper.deleteTbRepairData(tbVerifyLog);
	}
	
	
	/**
	 * 【删除repairTask数据】
	 */
	public void deleteVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepair){
		tbVerifyRepairMapper.deleteVerifyRepairTask(tbVerifyRepair);
	}
	
	
	 /**
	   * 【查询修复数据记录】
	   */
	  public List<TbVerifyRepair> findVerifyRepair(String dataSourceCode,TbVerifyRepair tbVerifyRepair){
		  return tbVerifyRepairMapper.findVerifyRepair(tbVerifyRepair);
	  }
	  /**
		* 【查询所有修复数据数量】
	  */
	  public int findVerifyRepairCount(String dataSourceCode,TbVerifyRepair tbVerifyRepair){
		  return tbVerifyRepairMapper.findVerifyRepairCount(tbVerifyRepair);
	  }
	

}
