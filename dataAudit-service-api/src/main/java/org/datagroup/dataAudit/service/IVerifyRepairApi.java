package org.datagroup.dataAudit.service;

import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;

public interface IVerifyRepairApi {
	public void insertVerifyRepair(String dataSourceCode,List<TbVerifyRepair> repairList);

	/**
	 * 【查询修复状态】
	 */
	public TbVerifyRepairTask findVerifyRepairTaskById(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask);
	
	
	/**
	 * 【插入修复状态记录】
	 */
	public void insertVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask);
	
	
	/**
	 * 【修改修复状态记录】
	 */
	public void updateVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepairTask);
	
	
	/**
	 * 【查询待修复数据】
	 */
	public List<TbVerifyRepair> findVerifyRepairLimit(String dataSourceCode,TbVerifyLog tbVerifyLog);
	
	
	/**
	 * 【批量修改稽核记录恢复状态】
	 */
	public void updateVerifyLogRepairStatus(String dataSourceCode,Map<String,Object> paramMap);
	
	/**
	 * 【批量插入恢复记录表】
	 */
	public void insertBatchRepairTask(List<TbVerifyRepairTask> tbVerifyRepairTaskList);
	
	
	/**
	 * 【修复更具inc表table_row_id】
	 * @param tableRowids
	 */
	public void updateSiebelIncByTableRowIds(String dataSourceCode,Map<String,Object> paramMap);
	
	
	/**
	 * 【delete verifyLog】
	 */
	public void deleteBatchVerifyLog(String dataSourceCode,Map<String,Object> paramMap);
	

	/**
	 * 【批量更改重置推送状态】
	 */
	public void updateRepairStatusByVerifyLog(String dataSourceCode,TbVerifyLog tbVerifyLog);
	
	/**
	 * 【删除修复记录表信息】
	 */
	public void deleteTbRepairData(String dataSourceCode,TbVerifyLog tbVerifyLog);
	
	
	/**
	 * 【删除repairTask数据】
	 */
	public void deleteVerifyRepairTask(String dataSourceCode,TbVerifyRepairTask tbVerifyRepair);
	
	
	 /**
	   * 【查询修复数据记录】
	   */
	  public List<TbVerifyRepair> findVerifyRepair(String dataSourceCode,TbVerifyRepair tbVerifyRepair);
	  /**
		* 【查询所有修复数据数量】
	  */
	  public int findVerifyRepairCount(String dataSourceCode,TbVerifyRepair tbVerifyRepair);
	
	
}
