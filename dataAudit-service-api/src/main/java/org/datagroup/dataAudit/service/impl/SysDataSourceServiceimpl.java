package org.datagroup.dataAudit.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.basepo.ResponseInfo;
import org.datagroup.dataAudit.common.exception.ResponseEnum;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;
import org.datagroup.dataAudit.config.InitConfigProperty;
import org.datagroup.dataAudit.dao.db.dynamicdb.ReloadDataSource;
import org.datagroup.dataAudit.dao.mapper.SysDataSourceMapper;
import org.datagroup.dataAudit.service.ISysDataSourceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

/**
 * 处理业务逻辑类
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Service(interfaceClass = ISysDataSourceApi.class, retries = 1,timeout = 100000)
@Component
public class SysDataSourceServiceimpl implements ISysDataSourceApi {

  private static final Logger logger = LogManager.getLogger(SysDataSourceServiceimpl.class);
  @Autowired
  @Qualifier("initConfigProperty")
  private InitConfigProperty initConfigProperty;

  @Autowired
  ConfigurableApplicationContext applicationContext;

  @Autowired
  SysDataSourceMapper tbSysDataSourceMapper;

  @Override
  public ResponseInfo<List<SysDataSourceInfo>> query(String searchParam) {
    // TODO Auto-generated method stub
    List<SysDataSourceInfo> dsList = tbSysDataSourceMapper.query(searchParam);
    if (dsList != null) {
      return new ResponseInfo<List<SysDataSourceInfo>>(ResponseEnum.SYS_SUCCESS, dsList);
    } else {
      return new ResponseInfo<List<SysDataSourceInfo>>(ResponseEnum.SYS_DATANUll);
    }
  }

  @Override
  public ResponseInfo addDs(SysDataSourceInfo sysDataSource) {
    // TODO Auto-generated method stub
    try {
      if ("oracle".equals(sysDataSource.getBase_type().toUpperCase())) {
        sysDataSource.setBase_dcn(initConfigProperty.getOracleBaseDcn());
        sysDataSource.setBase_url(initConfigProperty.getOrcbaseUrlBySn().replace("#ip", sysDataSource.getBase_ip()).replace("#port", sysDataSource.getBase_port()).replace("#SERVERNAME",
            sysDataSource.getBase_name()));
      } else {
        sysDataSource.setBase_dcn(initConfigProperty.getMysqlBaseDcn());
        sysDataSource.setBase_url(initConfigProperty.getMysqlbaseUrl().replace("#ip", sysDataSource.getBase_ip()).replace("#port", sysDataSource.getBase_port()).replace("#baseName",
            sysDataSource.getBase_name()));
      }
      sysDataSource.setStatus(initConfigProperty.getBaseStatus());
      tbSysDataSourceMapper.insert(sysDataSource);
      return new ResponseInfo(ResponseEnum.SYS_SUCCESS);
    } catch (Exception e) {
      logger.error("添加数据源[{}]出错[{}]", sysDataSource.getBase_code(), e.getStackTrace());
      return new ResponseInfo(ResponseEnum.SYS_FAILD);
    }
  }

  @Override
  public ResponseInfo delDs(String baseCode) {
    // TODO Auto-generated method stub
    tbSysDataSourceMapper.delete(baseCode);
    return new ResponseInfo(ResponseEnum.SYS_SUCCESS);
  }

  @Override
  public ResponseInfo editDs(SysDataSourceInfo sysDataSource) {
    // TODO Auto-generated method stub
    try {
      if ("oracle".equals(sysDataSource.getBase_type().toUpperCase())) {
        sysDataSource.setBase_dcn(initConfigProperty.getOracleBaseDcn());
        sysDataSource.setBase_url(initConfigProperty.getOrcbaseUrlBySn().replace("#ip", sysDataSource.getBase_ip()).replace("#port", sysDataSource.getBase_port()).replace("#SERVERNAME",
            sysDataSource.getBase_name()));
      } else {
        sysDataSource.setBase_dcn(initConfigProperty.getMysqlBaseDcn());
        sysDataSource.setBase_url(initConfigProperty.getMysqlbaseUrl().replace("#ip", sysDataSource.getBase_ip()).replace("#port", sysDataSource.getBase_port()).replace("#baseName",
            sysDataSource.getBase_name()));
      }
      sysDataSource.setStatus(initConfigProperty.getBaseStatus());
      tbSysDataSourceMapper.update(sysDataSource);
      return new ResponseInfo(ResponseEnum.SYS_SUCCESS);
    } catch (Exception e) {
      logger.error("添加数据源[{}]出错[{}]", sysDataSource.getBase_code(), e.getStackTrace());
      return new ResponseInfo(ResponseEnum.SYS_FAILD);
    }
  }

  @Override
  public ResponseInfo createDs(String baseCode, String baseStatus) {
    // TODO Auto-generated method stub
    ReloadDataSource reloadDs = applicationContext.getBean("reloadDataSource", ReloadDataSource.class);
    int result = reloadDs.verifyDs(baseCode, baseStatus);
    if (result == 0) {
      try {
        SysDataSourceInfo sysDataSource = new SysDataSourceInfo();
        sysDataSource.setBase_code(baseCode);
        sysDataSource.setStatus(baseStatus);
        tbSysDataSourceMapper.update(sysDataSource);
        return new ResponseInfo(ResponseEnum.SYS_FAILD);
      } catch (Exception e) {
        logger.debug("修改数据源[{}]状态失败", baseCode, e.getStackTrace());
        return new ResponseInfo(ResponseEnum.SYS_FAILD);
      }
    } else if (result == 1) {
      return new ResponseInfo(ResponseEnum.DS_EXIST);
    } else if (result == -1) {
      return new ResponseInfo(ResponseEnum.SYS_FAILD);
    } else {
      return new ResponseInfo(ResponseEnum.ERROR_PARAM_NULL);
    }
  }

}
