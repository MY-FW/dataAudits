package org.datagroup.dataAudit.service;

import org.datagroup.dataAudit.common.po.SysUserInfo;

public interface ISysUserInfoApi {

  public SysUserInfo findByParam(String account, String password, String userName);

}
