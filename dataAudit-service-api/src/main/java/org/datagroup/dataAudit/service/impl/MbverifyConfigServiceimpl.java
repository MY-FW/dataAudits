package org.datagroup.dataAudit.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.dao.mapper.VerifyConfigMapper;
import org.datagroup.dataAudit.service.IVerifyConfigApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

/**
 * 处理业务逻辑类
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Service(interfaceClass = IVerifyConfigApi.class, retries = 1,timeout = 1000000)
@Component
public class MbverifyConfigServiceimpl implements IVerifyConfigApi {

  private static final Logger logger = LogManager.getLogger(MbverifyConfigServiceimpl.class);

  @Autowired
  VerifyConfigMapper verifyConfigMapper;

  @Override
  public List<TbVerifyConfig> findVerifyConfigPageAll(String tbVerifyConfig) {
    return verifyConfigMapper.findVerifyConfigPageAll(tbVerifyConfig);
  }

  /*
   * @Override public int findVerifyConfigCount(TbVerifyConfig tbVerifyConfig) {
   * // TODO Auto-generated method stub return
   * verifyConfigMapper.findVerifyConfigCount(tbVerifyConfig); }
   */

  @Override
  public TbVerifyConfig getTbVerifyConfigList(String verify_id) {
    return verifyConfigMapper.getTbVerifyConfigList(verify_id);

  }

  @Override
  public int updateTbVerifyConfig(TbVerifyConfig tbVerifyConfig) {
    // TODO Auto-generated method stub
    return verifyConfigMapper.updateTbVerifyConfig(tbVerifyConfig);
  }

  @Override
  public int delete(String id) {
    // TODO Auto-generated method stub
    return verifyConfigMapper.delete(id);
  }

  @Override
  public int TbVerifyConfigadd(TbVerifyConfig tbVerifyConfig) {
    // TODO Auto-generated method stub
    return verifyConfigMapper.TbVerifyConfigadd(tbVerifyConfig);
  }
}
