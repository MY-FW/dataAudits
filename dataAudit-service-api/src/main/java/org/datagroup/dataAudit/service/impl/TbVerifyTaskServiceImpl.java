package org.datagroup.dataAudit.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyTask;
import org.datagroup.dataAudit.dao.mapper.TbVerifyTaskMapper;
import org.datagroup.dataAudit.service.ITbVerifyTaskApi;


import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.dao.db.dynamicdb.TargetDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

@Service(interfaceClass = ITbVerifyTaskApi.class, retries = 0,timeout = 1000000)
@Component
public class TbVerifyTaskServiceImpl implements ITbVerifyTaskApi {
  private static final Logger logger = LogManager.getLogger(TbVerifyTaskServiceImpl.class);
  @Autowired
  private TbVerifyTaskMapper tbVerifyTaskMapper;

   /**
	 * 【查询稽核任务总量】
	 */
  public int findVerifyTaskCount(String dataSourceCode, TbVerifyTask tbVerifyTask) { 
	  return this.tbVerifyTaskMapper.findVerifyTaskCount(tbVerifyTask);
  }
 
   /**
	 * 【查询稽核任务task信息到界面】
	 */
  public List<TbVerifyTask> findVerifyTaskPageAll(String dataSourceCode, TbVerifyTask tbVerifyTask)
  {
    return this.tbVerifyTaskMapper.findVerifyTaskPageAll(tbVerifyTask);
  }

  
  
  /*************************【稽核业务】*********************************/
	/**
	 * 【初始化查询所有待启动稽核并开始任务】
	 * 【当程序重新启动时加载稽核配置信息并启动稽核】
	 */
  
  public List<TbVerifyConfig> initTaskVerifyConfig(String dataSourceCode)
  {
    return this.tbVerifyTaskMapper.initTaskVerifyConfig();
  }

  
	/**
	 * 【查询数据类型到数据缓存】
	 */
  @TargetDataSource
  public List<DataColumnType> findTableColumnDataType(String dataSourceCode, DataColumnType dataColumnType)
  {
    return this.tbVerifyTaskMapper.findTableColumnDataType(dataColumnType);
  }

  
   /**
	 * 【查询增量inc表信息】【不分页为稽核初始化准备】
	 */
  @TargetDataSource
  public List<SiebelIncPojo> findSiebelIncList(String dataSourceCode, TbVerifyConfig tbVerifyConfig)
  {
    return this.tbVerifyTaskMapper.findSiebelIncList(tbVerifyConfig);
  }

   /**
	 * 【先查询task日志表是否有记录】
	 */
  public TbVerifyTask findTaskByVerifyId(String dataSourceCode, TbVerifyTask tbVerifyTask)
  {
    return this.tbVerifyTaskMapper.findTaskByVerifyId(tbVerifyTask);
  }

  
  /**
	 * 【插入task记录信息】
	 */
  public void insertVerifyTask(String dataSourceCode, TbVerifyTask tbVerifyTask)
  {
    this.tbVerifyTaskMapper.insertVerifyTask(tbVerifyTask);
  }

  
  /**
	 * 【修改任务状态单挑 upate Task】
	 */
  public void updateVerifyTask(String dataSourceCode, TbVerifyTask tbVerifyTask)
  {
    this.tbVerifyTaskMapper.updateVerifyTask(tbVerifyTask);
  }

  

	/**
	 * 【查询稽核目标未知表总量】
	 */
  @TargetDataSource
  public int findTargetVerifyDeleteCount(String dataSourceCode, Map<String, Object> paramMap)
  {
    return this.tbVerifyTaskMapper.findTargetVerifyDeleteCount(paramMap);
  }

  
   /**
	 * 【调用存储过程】
	 */
  @TargetDataSource
  public String callFunction(String dataSourceCode, Map<String, Object> paramMap)
  {
    return this.tbVerifyTaskMapper.callFunction(paramMap);
  }

  
  /**
	 * 【查询目标表数据结果】
	 */
  @TargetDataSource
  public List<Map<String, Object>> findVerifyDataList(String dataSourceCode, Map<String, Object> paramMap)
  {
    return this.tbVerifyTaskMapper.findVerifyDataList(paramMap);
  }

  
  
  /**
	 * 【稽核日志插入】
	 */
  public void insertBatchVerifyLog(String dataSourceCode, List<TbVerifyLog> tbVerifyLogList)
  {
    this.tbVerifyTaskMapper.insertBatchVerifyLog(tbVerifyLogList);
  }

  
  /**
	 * 【批量更改增量inc 的状态】
	 */
  @TargetDataSource
  public void updateSiebelIncStatus(String dataSourceCode, Map<String, Object> paramMap)
  {
    this.tbVerifyTaskMapper.updateSiebelIncStatus(paramMap);
  }

  
  
  /**
	 * 【查询配置信息单条】
	 */
  public List<TbVerifyConfig> findVerifyConfgByIDs(String dataSourceCode, List<Integer> verifyIds)
  {
    return this.tbVerifyTaskMapper.findVerifyConfgByIDs(verifyIds);
  }

  
  /**
	 *【批量更改稽核信息状态】
	 */
  public void updateVerifyConfigRunStatus(String dataSourceCode, List<Integer> verifyIds)
  {
    this.tbVerifyTaskMapper.updateVerifyConfigRunStatus(verifyIds);
  }

  
   /**
	 * 【初始化稽核配置信息】
	 */
  public List<TbVerifyConfig> findVerifyConfigCache(String dataSourceCode)
  {
    return this.tbVerifyTaskMapper.findVerifyConfigCache();
  }

  
   /**
	 * 【重置inc增量表状态信息】
	 */
  @TargetDataSource
  public void updateResetIncStatus(String dataSourceCode, TbVerifyConfig tbVerifyConfig)
  {
    this.tbVerifyTaskMapper.updateResetIncStatus(tbVerifyConfig);
  }
  
  
}
