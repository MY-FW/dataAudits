package org.datagroup.dataAudit.service;

import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyTask;
import org.datagroup.dataAudit.common.util.Page;

public interface ITbVerifyTaskApi {
	
	/**
	 * 【查询稽核任务总量】
	 */
	public int findVerifyTaskCount(String dataSourceCode,TbVerifyTask tbVerifyTask);
	
	
	/**
	 * 【查询稽核任务task信息到界面】
	 */
	public List<TbVerifyTask> findVerifyTaskPageAll(String dataSourceCode,TbVerifyTask tbVerifyTask);
	
	
	
	
	/*************************【稽核业务】*********************************/
	/**
	 * 【初始化查询所有待启动稽核并开始任务】
	 * 【当程序重新启动时加载稽核配置信息并启动稽核】
	 */
	public List<TbVerifyConfig> initTaskVerifyConfig(String dataSourceCode);
	
	
	
	/**
	 * 【查询数据类型到数据缓存】
	 */
	public List<DataColumnType> findTableColumnDataType(String dataSourceCode,DataColumnType dataColumnType);
	
	
	
	/**
	 * 【查询增量inc表信息】【不分页为稽核初始化准备】
	 */
	public List<SiebelIncPojo> findSiebelIncList(String dataSourceCode,TbVerifyConfig tbVerifyConfig);
	
	
	/**
	 * 【先查询task日志表是否有记录】
	 */
	public TbVerifyTask findTaskByVerifyId(String dataSourceCode,TbVerifyTask tbVerifyTask);
	
	
	/**
	 * 【插入task记录信息】
	 */
	public void insertVerifyTask(String dataSourceCode,TbVerifyTask tbVerifyTask);
	
	
	/**
	 * 【修改任务状态单挑 upate Task】
	 */
	public void updateVerifyTask(String dataSourceCode,TbVerifyTask tbVerifyTask);
	
	

	/**
	 * 【查询稽核目标未知表总量】
	 */
	public int findTargetVerifyDeleteCount(String dataSourceCode,Map<String,Object> paramMap);
	
	
	/**
	 * 【调用存储过程】
	 */
	public String callFunction(String dataSourceCode,Map<String,Object> paramMap);
	
	
	/**
	 * 【查询目标表数据结果】
	 */
	public List<Map<String,Object>> findVerifyDataList(String dataSourceCode,Map<String,Object> paramMap);
	
	
	
	/**
	 * 【稽核日志插入】
	 */
	public void insertBatchVerifyLog(String dataSourceCode,List<TbVerifyLog> tbVerifyLogList);
	
	
	/**
	 * 【批量更改增量inc 的状态】
	 */
	public void updateSiebelIncStatus(String dataSourceCode,Map<String,Object> paramMap);
	
	
	
	/**
	 * 【查询配置信息单条】
	 */
	public List<TbVerifyConfig> findVerifyConfgByIDs(String dataSourceCode,List<Integer> verifyIds);
	
	
	/**
	 *【批量更改稽核信息状态】
	 */
	public void updateVerifyConfigRunStatus(String dataSourceCode,List<Integer> verifyIds);
	
	
	/**
	 * 【初始化稽核配置信息】
	 */
	public List<TbVerifyConfig> findVerifyConfigCache(String dataSourceCode);
	
	
	/**
	 * 【重置inc增量表状态信息】
	 */
	public void updateResetIncStatus(String dataSourceCode,TbVerifyConfig tbVerifyConfig);
	
	
}
