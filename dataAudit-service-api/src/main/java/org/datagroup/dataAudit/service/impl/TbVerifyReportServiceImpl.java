package org.datagroup.dataAudit.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;
import org.datagroup.dataAudit.dao.mapper.TbVerifyLogMapper;
import org.datagroup.dataAudit.service.IVerifyReportApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

@Service(interfaceClass = IVerifyReportApi.class, timeout = 1000000)
@Component
public class TbVerifyReportServiceImpl implements IVerifyReportApi {
  private static final Logger logger = LogManager.getLogger(TbVerifyReportServiceImpl.class);
  @Autowired
  private TbVerifyLogMapper tbVerifyLogMapper;

  @Override
  public List<TbVerifyLog> findLogGroupBy(String dataSourceCode, TbVerifyLog tbVerifyLog) {
    // TODO Auto-generated method stub
    return tbVerifyLogMapper.findLogGroupBy(dataSourceCode, tbVerifyLog);
  }

  @Override
  public int findLogGroupByCount(String dataSourceCode, TbVerifyLog tbVerifyLog) {
    return tbVerifyLogMapper.findLogGroupByCount(dataSourceCode, tbVerifyLog);
  }

  @Override
  public int findDetailCount(String dataSourceCode, TbVerifyLog tbVerifyLog) {
    // TODO Auto-generated method stub
    return tbVerifyLogMapper.findDetailCount(tbVerifyLog);
  }

  @Override
  public List<TbVerifyLog> findDetail(String dataSourceCode, TbVerifyLog tbVerifyLog) {
    // TODO Auto-generated method stub
    return tbVerifyLogMapper.findDetail(tbVerifyLog);
  }

  @Override
  public List<TbVerifyLog> findVerifyLogByRepair(String dataSourceCode, List<TbVerifyLog> tbVerifyLogs) {
    // TODO Auto-generated method stub
    return tbVerifyLogMapper.findVerifyLogByRepair(dataSourceCode, tbVerifyLogs);
  }

  @Override
  public void updateTbVerifyLog(String dataSourceCode, List<TbVerifyRepair> tbVerifyRepairs) {
    tbVerifyLogMapper.updateTbVerifyLog(dataSourceCode, tbVerifyRepairs);

  }

  @Override
  public int findRepairTaskCount(String dataSourceCode, TbVerifyRepairTask tbVerifyRepairTask) {
    return tbVerifyLogMapper.findRepairTaskCount(dataSourceCode, tbVerifyRepairTask);
  }

  @Override
  public List<TbVerifyRepairTask> findRepairTask(String dataSourceCode, TbVerifyRepairTask tbVerifyRepairTask) {
    return tbVerifyLogMapper.findRepairTask(dataSourceCode, tbVerifyRepairTask);
  }

  /**
   * 【根据原表信息和目标表信息查询配置信息】
   */
  @Override
  public TbVerifyConfig findVerifyConfigByThing(String dataSourceCode, TbVerifyRepair tbVerifyRepair) {
    return tbVerifyLogMapper.findVerifyConfigByThing(tbVerifyRepair);
  }
}
