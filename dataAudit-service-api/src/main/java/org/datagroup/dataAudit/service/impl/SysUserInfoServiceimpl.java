package org.datagroup.dataAudit.service.impl;

import org.datagroup.dataAudit.common.po.SysUserInfo;
import org.datagroup.dataAudit.dao.mapper.SysUserInfoMapper;
import org.datagroup.dataAudit.service.ISysUserInfoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

/**
 * 处理业务逻辑类
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Service(interfaceClass = ISysUserInfoApi.class, retries = 1)
@Component
public class SysUserInfoServiceimpl implements ISysUserInfoApi {

  @Autowired
  SysUserInfoMapper sysUserInfoMapper;

  @Override
  public SysUserInfo findByParam(String account, String password, String userName) {
    // TODO Auto-generated method stub
    return sysUserInfoMapper.findByParam(account, password, userName);
  }

}
