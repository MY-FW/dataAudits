package org.datagroup.dataAudit.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;

/**
 * 查询配置表（VerifyConfig）数据，并开启缓存
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Mapper
public interface VerifyConfigMapper {

	/*public int findVerifyConfigCount(TbVerifyConfig tbVerifyConfig);*/

	public List<TbVerifyConfig> findVerifyConfigPageAll(@Param("tbVerifyConfig") String tbVerifyConfig);

	public TbVerifyConfig getTbVerifyConfigList(String verify_id);

	public int updateTbVerifyConfig(TbVerifyConfig tbVerifyConfig);

	public int delete(String id);
	
	public int TbVerifyConfigadd(TbVerifyConfig tbVerifyConfig);
	
}
