package org.datagroup.dataAudit.dao.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;

@Mapper
public interface TbVerifyRepairMapper {
	public void insertVerifyRepair(List<TbVerifyRepair> repairList);

	/**
	 * 【查询修复状态】
	 */
	public TbVerifyRepairTask findVerifyRepairTaskById(TbVerifyRepairTask tbVerifyRepairTask);
	
	
	/**
	 * 【插入修复状态记录】
	 */
	public void insertVerifyRepairTask(TbVerifyRepairTask tbVerifyRepairTask);
	
	
	/**
	 * 【修改修复状态记录】
	 */
	public void updateVerifyRepairTask(TbVerifyRepairTask tbVerifyRepairTask);
	
	
	/**
	 * 【查询待修复数据】
	 */
	public List<TbVerifyRepair> findVerifyRepairLimit(TbVerifyLog tbVerifyLog);
	
	
	/**
	 * 【批量修改稽核记录恢复状态】
	 */
	public void updateVerifyLogRepairStatus(Map<String,Object> paramMap);
	
	/**
	 * 【批量插入恢复记录表】
	 */
	public void insertBatchRepairTask(List<TbVerifyRepairTask> tbVerifyRepairTaskList);
	
	
	/**
	 * 【修复更具inc表table_row_id】
	 * @param tableRowids
	 */
	public void updateSiebelIncByTableRowIds(Map<String,Object> paramMap);
	
	
	/**
	 * 【delete verifyLog】
	 */
	public void deleteBatchVerifyLog(Map<String,Object> paramMap);
	

	/**
	 * 【批量更改重置推送状态】
	 */
	public void updateRepairStatusByVerifyLog(TbVerifyLog tbVerifyLog);
	
	/**
	 * 【删除修复记录表信息】
	 */
	public void deleteTbRepairData(TbVerifyLog tbVerifyLog);
	
	
	/**
	 * 【删除repairTask数据】
	 */
	public void deleteVerifyRepairTask(TbVerifyRepairTask tbVerifyRepair);
	
	
	 /**
	   * 【查询修复数据记录】
	   */
	  public List<TbVerifyRepair> findVerifyRepair(TbVerifyRepair tbVerifyRepair);
	  /**
		* 【查询所有修复数据数量】
	  */
	  public int findVerifyRepairCount(TbVerifyRepair tbVerifyRepair);
}
