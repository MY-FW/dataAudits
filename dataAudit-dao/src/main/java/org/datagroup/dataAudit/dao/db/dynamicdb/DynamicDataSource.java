package org.datagroup.dataAudit.dao.db.dynamicdb;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态加载数据源
 *
 * @author Chentw
 * @version
 * @版权: 版权所有 (c) 2018
 * @see:
 * @创建日期: 2018年11月13日
 * @功能说明:
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

  private static final Logger logger = LogManager.getLogger(DynamicDataSource.class);
  public static Map<Object, Object> dataSourceMap = new HashMap<Object, Object>();
  private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

  /**
   * @Description:设置数据源的变量
   * @param dataSourceName
   * @return void
   * @author Chentw
   * @date 2018年11月23日
   */
  public static void setDataSource(String dataSourceName) {
    contextHolder.set(dataSourceName);
  }

  public static String getDataSource() {
    return contextHolder.get();
  }

  public static void clearDataSource() {
    contextHolder.remove();
  }

  @Override
  protected Object determineCurrentLookupKey() {
    // TODO Auto-generated method stub
    return getDataSource();
  }

  @Override
  public void setTargetDataSources(Map<Object, Object> targetDataSources) {
    // targetDataSources.forEach((key, value) -> {
    // logger.debug("打印所有数据源Key：[{}],Value：[{}]", key, value);
    // });
    super.setTargetDataSources(targetDataSources);
    super.afterPropertiesSet();
  }

  /**
   * @Description:判断指定DataSrouce当前是否存在
   * @param dataSource
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年11月23日
   */
  public static boolean containsDataSource(String dataSource) {
    return dataSourceMap.containsKey(dataSource);
  }
}
