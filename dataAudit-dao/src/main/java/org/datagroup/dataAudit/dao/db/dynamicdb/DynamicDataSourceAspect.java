package org.datagroup.dataAudit.dao.db.dynamicdb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Order;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Order(1)
@Component
public class DynamicDataSourceAspect {
  private Logger logger = LogManager.getLogger(DynamicDataSourceAspect.class);

  /**
   * @Description:切换数据库
   * @param point
   * @param targetDataSource
   * @return void
   * @author Chentw
   * @date 2018年11月23日
   */
  @Before("@annotation(targetDataSource)")
  public void changeDataSource(JoinPoint point, TargetDataSource targetDataSource) throws Throwable {
    String dsName = (String) point.getArgs()[0];
    if (!DynamicDataSource.containsDataSource(dsName)) {
      logger.error("数据源[{}]不存在，使用默认数据源 > {}", dsName, point.getSignature());
    } else {
      logger.debug("Use DataSource : {} > {}", dsName, point.getSignature());
      DynamicDataSource.setDataSource(dsName);
    }
  }

  /**
   * @Description:销毁数据源 在所有的方法执行执行完毕后
   * @param point
   * @param targetDataSource
   * @return void
   * @author Chentw
   * @date 2018年11月23日
   */
  @After("@annotation(targetDataSource)")
  public void destroyDataSource(JoinPoint point, TargetDataSource targetDataSource) {
    String dsName = (String) point.getArgs()[0];
    logger.debug("Revert DataSource : {} > {}", dsName, point.getSignature());
    DynamicDataSource.clearDataSource();
  }

}
