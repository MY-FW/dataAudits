package org.datagroup.dataAudit.dao.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyRepair;
import org.datagroup.dataAudit.common.po.TbVerifyRepairTask;

@Mapper
public interface TbVerifyLogMapper {

  public List<TbVerifyLog> findLogGroupBy(String dataSourceCode,@Param("tbVerifyLog")TbVerifyLog tbVerifyLog);
  public int findLogGroupByCount(String dataSourceCode,@Param("tbVerifyLog")TbVerifyLog tbVerifyLog);
  public int findDetailCount(TbVerifyLog tbVerifyLog);
  public List<TbVerifyLog> findDetail(TbVerifyLog tbVerifyLog);
  public List<TbVerifyLog> findVerifyLogByRepair(String dataSourceCode,@Param("tbVerifyLog")List<TbVerifyLog> tbVerifyLogs);
  public void updateTbVerifyLog(String dataSourceCode,@Param("tbVerifyRepairs")List<TbVerifyRepair> tbVerifyRepairs);
  public int findRepairTaskCount(String dataSourceCode,@Param("tbVerifyRepairTask")TbVerifyRepairTask tbVerifyRepairTask);
  public List<TbVerifyRepairTask> findRepairTask(String dataSourceCode,@Param("tbVerifyRepairTask")TbVerifyRepairTask tbVerifyRepairTask);

  /**
	 * 【根据原表信息和目标表信息查询配置信息】
	 */
	public TbVerifyConfig findVerifyConfigByThing(TbVerifyRepair tbVerifyRepair);
}
