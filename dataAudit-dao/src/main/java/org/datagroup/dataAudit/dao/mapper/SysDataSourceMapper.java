package org.datagroup.dataAudit.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;

/**
 * 查询配置表（VerifyConfig）数据，并开启缓存
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Mapper
public interface SysDataSourceMapper {

  List<SysDataSourceInfo> query(@Param("searchParam") String searchParam);

  void insert(SysDataSourceInfo sysDataSource);

  void update(SysDataSourceInfo sysDataSource);

  void delete(@Param("baseCode") String baseCode);

}
