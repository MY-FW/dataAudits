package org.datagroup.dataAudit.dao.db.basedb;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.dao.db.dynamicdb.DynamicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;

/**
 * 多数据源，集成druid,配置监控
 * 
 * @author Chentw
 * @creation 2018年9月28日
 */
@Configuration
@EnableTransactionManagement
public class InitPrimaryDataSource {
  private static final Logger logger = LogManager.getLogger(InitPrimaryDataSource.class);

  @Bean("mysqlDataSource")
  @Qualifier("mysqlDataSource")
  @ConfigurationProperties(prefix = "spring.datasource.mysql")
  public DataSource mysqlDataSource(Environment environment) {
    return DruidDataSourceBuilder.create().build(environment, "spring.datasource.druid.");
  }

  @Bean(name = "mysqlJdbcTemplate")
  public JdbcTemplate mysqlJdbcTemplate(@Qualifier("mysqlDataSource") DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  @Bean(name = "mysqlTransactionManager")
  public DataSourceTransactionManager mysqlTransactionManager(@Qualifier("mysqlDataSource") DataSource mysqlDataSource) {
    DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
    dataSourceTransactionManager.setDataSource(mysqlDataSource);
    return dataSourceTransactionManager;
  }

  @Bean
  @Primary
  public DataSource dynamicDataSource(@Qualifier("mysqlDataSource") DataSource mysqlDataSource) {
    DynamicDataSource dynamicDataSource = new DynamicDataSource();
    dynamicDataSource.dataSourceMap.put("mysqlDataSource", mysqlDataSource);
    dynamicDataSource.setTargetDataSources(dynamicDataSource.dataSourceMap);
    // 设置默认数据源，父类的方法
    dynamicDataSource.setDefaultTargetDataSource(mysqlDataSource);
    return dynamicDataSource;
  }

}
