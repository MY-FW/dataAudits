package org.datagroup.dataAudit.dao.db.dynamicdb;

import java.sql.DriverManager;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.SysDataSourceInfo;
import org.datagroup.dataAudit.dao.mapper.SysDataSourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.stat.DruidDataSourceStatManager;

/**
 * 初始化加载动态数据源
 *
 * @author Chentw
 * @version
 * @版权: 版权所有 (c) 2018
 * @see:
 * @创建日期: 2018年11月13日
 * @功能说明:
 */
@Configuration
public class ReloadDataSource {

  private static final Logger logger = LogManager.getLogger(ReloadDataSource.class);
  @Autowired
  ConfigurableApplicationContext applicationContext;

  /**
   * @Description:初始化数据源
   * @return void
   * @author Chentw
   * @date 2018年12月4日
   */
  @PostConstruct
  public void reloadDataSource() {
    createDataSource("YQY");
  }

  public int verifyDs(String searchParam, String baseStatus) {
    if (baseStatus != null && baseStatus.equals("YQY")) {
      DynamicDataSource dynamicDataSource = applicationContext.getBean("dynamicDataSource", DynamicDataSource.class);
      if (!dynamicDataSource.dataSourceMap.containsKey(searchParam)) {
        boolean result = createDataSource(searchParam);
        if (result) {
          return 0;
        } else {
          return -1;
        }
      } else {
        return 1;
      }
    } else {
      return -2;
    }

  }

  /**
   * @Description:验证数据库连接
   * @param driveClass
   * @param url
   * @param userName
   * @param password
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年12月10日
   */
  public boolean verifyConn(String driveClass, String url, String userName, String password) {
    try {
      Class.forName(driveClass);
      DriverManager.getConnection(url, userName, password);
    } catch (Exception e) {
      logger.info("数据源:[{}]连接测试未成功,请检查数据源配置是否正确[{}]", userName, e.getStackTrace());
      e.printStackTrace();
      return false;
    }
    return true;
  }

  /**
   * @Description: 动态加载数据源
   * @param dsCode
   * @param baseStatus
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年12月4日
   */
  public boolean createDataSource(String searchParam) {
    try {
      // DefaultListableBeanFactory defaultListableBeanFactory =
      // (DefaultListableBeanFactory)
      // applicationContext.getAutowireCapableBeanFactory();
      SysDataSourceMapper tbSysDataSourceMapper = applicationContext.getBean("sysDataSourceMapper", SysDataSourceMapper.class);
      List<SysDataSourceInfo> dataSourcelist = tbSysDataSourceMapper.query(searchParam);
      DynamicDataSource dynamicDataSource = applicationContext.getBean("dynamicDataSource", DynamicDataSource.class);
      for (SysDataSourceInfo tbSysDataSource : dataSourcelist) {
        try {
          if (verifyConn(tbSysDataSource.getBase_dcn(), tbSysDataSource.getBase_url(), tbSysDataSource.getUser_name(), tbSysDataSource.getPassword())) {
            logger.error("启动开始加载数据源,数据源编码：[{}]，数据源名称：[{}]", tbSysDataSource.getBase_code(), tbSysDataSource.getBase_name());
            BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(DruidDataSource.class);
            beanDefinitionBuilder.addPropertyValue("driverClassName", tbSysDataSource.getBase_dcn());
            beanDefinitionBuilder.addPropertyValue("url", tbSysDataSource.getBase_url());
            beanDefinitionBuilder.addPropertyValue("username", tbSysDataSource.getUser_name());
            beanDefinitionBuilder.addPropertyValue("password", tbSysDataSource.getPassword());
            beanDefinitionBuilder.addPropertyValue("initialSize", tbSysDataSource.getInitia_size());
            beanDefinitionBuilder.addPropertyValue("maxActive", 20);
            beanDefinitionBuilder.addPropertyValue("minIdle", 5);
            beanDefinitionBuilder.addPropertyValue("maxWait", 60000);
            beanDefinitionBuilder.addPropertyValue("timeBetweenEvictionRunsMillis", 60000);
            beanDefinitionBuilder.addPropertyValue("minEvictableIdleTimeMillis", 300000);
            beanDefinitionBuilder.addPropertyValue("testWhileIdle", true);
            beanDefinitionBuilder.addPropertyValue("testOnBorrow", false);
            beanDefinitionBuilder.addPropertyValue("testOnReturn", false);
            beanDefinitionBuilder.addPropertyValue("poolPreparedStatements", true);
            beanDefinitionBuilder.addPropertyValue("maxPoolPreparedStatementPerConnectionSize", 20);
            // beanDefinitionBuilder.addPropertyValue("filters",
            // "stat,wall,logback");
            beanDefinitionBuilder.addPropertyValue("useGlobalDataSourceStat", true);
            // beanDefinitionBuilder.addPropertyValue("statementExecutableSqlLogEnable",
            // true);
            beanDefinitionBuilder.addPropertyValue("connectionProperties", "druid.stat.mergeSql=true;druid.stat.slowSqlMillis=500");

            BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();
            BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) applicationContext.getBeanFactory();
            beanFactory.registerBeanDefinition(tbSysDataSource.getBase_code(), beanDefinition);

            // defaultListableBeanFactory.registerBeanDefinition(tbSysDataSource.getBase_code(),
            // beanDefinitionBuilder.getBeanDefinition());
            // DataSource createDataSource =
            // applicationContext.getBean(tbSysDataSource.getBase_code(),
            // DruidDataSource.class);
            dynamicDataSource.dataSourceMap.put(tbSysDataSource.getBase_code(), applicationContext.getBean(tbSysDataSource.getBase_code(), DataSource.class));
            logger.debug("加载数据源成功，数据源编码：[{}]，数据源名称：[{}]", tbSysDataSource.getBase_code(), tbSysDataSource.getBase_name());
          } else {
            return false;
          }
        } catch (Exception e) {
          logger.debug("加载数据源失败[{}]，数据源编码：[{}]，数据源名称：[{}]", e.getStackTrace(), tbSysDataSource.getBase_code(), tbSysDataSource.getBase_name());
          e.printStackTrace();
          return false;
        }
      }
      dynamicDataSource.setTargetDataSources(dynamicDataSource.dataSourceMap);
      return true;
    } catch (Exception e) {
      logger.error("加载数据源失败:{}", e.getStackTrace());
      return false;
    }
  }

  /**
   * @Description:删除数据源
   * @param datasourceId
   * @return
   * @return boolean
   * @author Chentw
   * @date 2018年11月23日
   */
  public boolean delDatasources(String dataSourceKey) {
    logger.info("开始删除数据源：[{}]。", dataSourceKey);
    DynamicDataSource dds = null;
    if (dds.dataSourceMap.containsKey(dataSourceKey)) {
      Set<DruidDataSource> druidDataSourceInstances = DruidDataSourceStatManager.getDruidDataSourceInstances();
      for (DruidDataSource druidDataSource : druidDataSourceInstances) {
        if (dataSourceKey.equals(druidDataSource.getName())) {
          dds.dataSourceMap.remove(dataSourceKey);
          DruidDataSourceStatManager.removeDataSource(druidDataSource);
          // 将map赋值给父类的TargetDataSources
          dds.setTargetDataSources(dds.dataSourceMap);
          logger.debug("删除数据源[{}]成功。", dataSourceKey);
          return true;
        }
      }
      logger.debug("删除数据源[{}]失败。", dataSourceKey);
      return false;
    } else {
      logger.debug("未找到[{}]数据源。", dataSourceKey);
      return false;
    }
  }

}
