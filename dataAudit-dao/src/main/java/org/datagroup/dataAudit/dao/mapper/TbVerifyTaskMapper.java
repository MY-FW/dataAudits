package org.datagroup.dataAudit.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;
import org.datagroup.dataAudit.common.po.TbVerifyTask;

/**
 * 
 * @author yannannan
 * @createTime 2018年11月29日
 * @updateTime 2018年11月29日
 * @desc 【稽核应用任务调度】
 */
@Mapper
public interface TbVerifyTaskMapper {
	
	/**
	 * 【查询稽核任务总量】
	 */

	public int findVerifyTaskCount(TbVerifyTask tbVerifyTask);
	
	
	/**
	 * 【执行调度任务功能】
	 * @param 批量配置信息id标识verify_id
	 */
	public void batchInsertVerifyTask(List<Integer> verifyConfigIds);
	
	public List<TbVerifyTask> findVerifyTaskPageAll(TbVerifyTask tbVerifyTask);
	
	
	
	/*************************【稽核业务】*********************************/
	/**
	 * 【初始化查询所有待启动稽核并开始任务】
	 * 【当程序重新启动时加载稽核配置信息并启动稽核】
	 */
	public List<TbVerifyConfig> initTaskVerifyConfig();
	
	
	
	/**
	 * 【查询数据类型到数据缓存】
	 */
	public List<DataColumnType> findTableColumnDataType(DataColumnType dataColumnType);
	
	
	
	/**
	 * 【查询增量inc表信息】【不分页为稽核初始化准备】
	 */
	public List<SiebelIncPojo> findSiebelIncList(TbVerifyConfig tbVerifyConfig);
	
	
	/**
	 * 【先查询task日志表是否有记录】
	 */
	public TbVerifyTask findTaskByVerifyId(TbVerifyTask tbVerifyTask);
	
	
	/**
	 * 【插入task记录信息】
	 */
	public void insertVerifyTask(TbVerifyTask tbVerifyTask);
	
	
	/**
	 * 【修改任务状态单挑 upate Task】
	 */
	public void updateVerifyTask(TbVerifyTask tbVerifyTask);
	
	

	/**
	 * 【查询稽核目标未知表总量】
	 */
	public int findTargetVerifyDeleteCount(Map<String,Object> paramMap);
	
	
	/**
	 * 【调用存储过程】
	 */
	public String callFunction(Map<String,Object> paramMap);
	
	
	/**
	 * 【查询目标表数据结果】
	 */
	public List<Map<String,Object>> findVerifyDataList(Map<String,Object> paramMap);
	
	
	
	/**
	 * 【稽核日志插入】
	 */
	public void insertBatchVerifyLog(List<TbVerifyLog> tbVerifyLogList);
	
	
	/**
	 * 【批量更改增量inc 的状态】
	 */
	public void updateSiebelIncStatus(Map<String,Object> paramMap);
	
	
	
	/**
	 * 【查询配置信息单条】
	 */
	public List<TbVerifyConfig> findVerifyConfgByIDs(List<Integer> verifyIds);
	
	
	/**
	 *【批量更改稽核信息状态】
	 */
	public void updateVerifyConfigRunStatus(List<Integer> verifyIds);
	
	
	/**
	 * 【初始化稽核配置信息】
	 */
	public List<TbVerifyConfig> findVerifyConfigCache();
	
	
	public void updateResetIncStatus(TbVerifyConfig tbVerifyConfig);
	
}
