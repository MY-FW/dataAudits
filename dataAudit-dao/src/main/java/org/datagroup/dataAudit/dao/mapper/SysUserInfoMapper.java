package org.datagroup.dataAudit.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.datagroup.dataAudit.common.po.SysUserInfo;

/**
 * 查询配置表（VerifyConfig）数据，并开启缓存
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Mapper
public interface SysUserInfoMapper {

  SysUserInfo findByParam(@Param("account") String account, @Param("password") String password, @Param("userName") String userName);

  void insert(SysUserInfo tbSysUser);

  void update(SysUserInfo tbSysUser);

  void delete(int userId);

}
