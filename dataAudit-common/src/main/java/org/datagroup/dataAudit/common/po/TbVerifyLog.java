package org.datagroup.dataAudit.common.po;

import java.io.Serializable;

public class TbVerifyLog extends BaseEntity2 implements Serializable{
	
	 private static final long serialVersionUID = 1L;
	 private Integer id;
	 private String row_id;//稽核inc中table_row_id
	 private String source_base_code;//原表数据源
	 private String target_base_code;//目标数据源
	 private String source_base_name;//原表数据库名称
	 private String target_base_name;//目标表数据库名称
	 private String source_table;//原表名称
	 private String source_inc_table;//inc增量表名称
	 private String source_column;//原表字段
	 private String source_value;//原表值
	 private String target_table;//目标名称
	 private String target_column;//目标表字段
	 private String target_value;//目标字段值
	 private String verify_status;//稽核状态【70A待稽核、70B稽核中、70C稽核失败、70E稽核异常】
	 private String verify_cd;//稽核结果【Y数据一直，N数据不一致】
	 private String create_time;//创建时间
	 private String last_update;//稽核结束时间
	 private Integer verify_type;//稽核结果类型稽核结果类型【0数据一致！、1值不匹配！、2源表字段空值！，目标字段有值、3源字段有值，目标表字段空值！、4目标数据不存在！、5条件不匹配，无需转换!，6源数据已删除，目标数据已存在，数据不一直!、7稽核异常】
	 private String verify_desc;
	 private String inc_row_id;//INC表ROWID主键
	 private String remarks;//
	 private Integer repair_status;//数据修复推送状态(0、未转移、1、已转移)
	 private Integer center_type;//表所属中心
	 private String op_type;//操作类型、update、delete
	 private Integer count;
	 private String start_time;
	 private String end_time;
	 
	public TbVerifyLog() {
		super();
	}
	public TbVerifyLog(Integer id, String row_id, String source_base_code,
			String target_base_code, String source_base_name,
			String target_base_name, String source_table,
			String source_inc_table, String source_column, String source_value,
			String target_table, String target_column, String target_value,
			String verify_status, String verify_cd, String create_time,
			String last_update, Integer verify_type, String verify_desc,
			String inc_row_id, String remarks, Integer repair_status,
			Integer center_type, String op_type, Integer count,
			String start_time, String end_time) {
		super();
		this.id = id;
		this.row_id = row_id;
		this.source_base_code = source_base_code;
		this.target_base_code = target_base_code;
		this.source_base_name = source_base_name;
		this.target_base_name = target_base_name;
		this.source_table = source_table;
		this.source_inc_table = source_inc_table;
		this.source_column = source_column;
		this.source_value = source_value;
		this.target_table = target_table;
		this.target_column = target_column;
		this.target_value = target_value;
		this.verify_status = verify_status;
		this.verify_cd = verify_cd;
		this.create_time = create_time;
		this.last_update = last_update;
		this.verify_type = verify_type;
		this.verify_desc = verify_desc;
		this.inc_row_id = inc_row_id;
		this.remarks = remarks;
		this.repair_status = repair_status;
		this.center_type = center_type;
		this.op_type = op_type;
		this.count = count;
		this.start_time = start_time;
		this.end_time = end_time;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRow_id() {
		return row_id;
	}
	public void setRow_id(String row_id) {
		this.row_id = row_id;
	}
	public String getSource_base_code() {
		return source_base_code;
	}
	public void setSource_base_code(String source_base_code) {
		this.source_base_code = source_base_code;
	}
	public String getTarget_base_code() {
		return target_base_code;
	}
	public void setTarget_base_code(String target_base_code) {
		this.target_base_code = target_base_code;
	}
	public String getSource_base_name() {
		return source_base_name;
	}
	public void setSource_base_name(String source_base_name) {
		this.source_base_name = source_base_name;
	}
	public String getTarget_base_name() {
		return target_base_name;
	}
	public void setTarget_base_name(String target_base_name) {
		this.target_base_name = target_base_name;
	}
	public String getSource_table() {
		return source_table;
	}
	public void setSource_table(String source_table) {
		this.source_table = source_table;
	}
	public String getSource_inc_table() {
		return source_inc_table;
	}
	public void setSource_inc_table(String source_inc_table) {
		this.source_inc_table = source_inc_table;
	}
	public String getSource_column() {
		return source_column;
	}
	public void setSource_column(String source_column) {
		this.source_column = source_column;
	}
	public String getSource_value() {
		return source_value;
	}
	public void setSource_value(String source_value) {
		this.source_value = source_value;
	}
	public String getTarget_table() {
		return target_table;
	}
	public void setTarget_table(String target_table) {
		this.target_table = target_table;
	}
	public String getTarget_column() {
		return target_column;
	}
	public void setTarget_column(String target_column) {
		this.target_column = target_column;
	}
	public String getTarget_value() {
		return target_value;
	}
	public void setTarget_value(String target_value) {
		this.target_value = target_value;
	}
	public String getVerify_status() {
		return verify_status;
	}
	public void setVerify_status(String verify_status) {
		this.verify_status = verify_status;
	}
	public String getVerify_cd() {
		return verify_cd;
	}
	public void setVerify_cd(String verify_cd) {
		this.verify_cd = verify_cd;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getLast_update() {
		return last_update;
	}
	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}
	public Integer getVerify_type() {
		return verify_type;
	}
	public void setVerify_type(Integer verify_type) {
		this.verify_type = verify_type;
	}
	public String getVerify_desc() {
		return verify_desc;
	}
	public void setVerify_desc(String verify_desc) {
		this.verify_desc = verify_desc;
	}
	public String getInc_row_id() {
		return inc_row_id;
	}
	public void setInc_row_id(String inc_row_id) {
		this.inc_row_id = inc_row_id;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Integer getRepair_status() {
		return repair_status;
	}
	public void setRepair_status(Integer repair_status) {
		this.repair_status = repair_status;
	}
	public Integer getCenter_type() {
		return center_type;
	}
	public void setCenter_type(Integer center_type) {
		this.center_type = center_type;
	}
	public String getOp_type() {
		return op_type;
	}
	public void setOp_type(String op_type) {
		this.op_type = op_type;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 

}
