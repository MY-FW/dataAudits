package org.datagroup.dataAudit.common.po;

import java.io.Serializable;

public class TbVerifyTask extends BaseEntity implements Serializable{
	
	 private static final long serialVersionUID = 1L;
	 private Integer task_id;//任务标识唯一主键
	 private Integer verify_id;//稽核配置外键id
	 private String thread_name;//线程名称
	 private String task_status;//70A待执行、70B执行中、70C执行完成、70D任务暂停、70E稽核异常
	 private String task_start_time;//任务创建时间
	 private String task_end_time;//任务结束时间
	 private String create_time;//任务创建时间
	 private long verify_count;
	 private String task_error_msg;//任务执行异常描述
	 private String verifyIds;//界面批量稽核配置ids
	 
	public TbVerifyTask() {
		super();
	}
	public TbVerifyTask(Integer task_id, Integer verify_id, String thread_name,
			String task_status, String task_start_time, String task_end_time,
			String create_time, long verify_count, String task_error_msg,
			String verifyIds) {
		super();
		this.task_id = task_id;
		this.verify_id = verify_id;
		this.thread_name = thread_name;
		this.task_status = task_status;
		this.task_start_time = task_start_time;
		this.task_end_time = task_end_time;
		this.create_time = create_time;
		this.verify_count = verify_count;
		this.task_error_msg = task_error_msg;
		this.verifyIds = verifyIds;
	}
	public Integer getTask_id() {
		return task_id;
	}
	public void setTask_id(Integer task_id) {
		this.task_id = task_id;
	}
	public Integer getVerify_id() {
		return verify_id;
	}
	public void setVerify_id(Integer verify_id) {
		this.verify_id = verify_id;
	}
	public String getThread_name() {
		return thread_name;
	}
	public void setThread_name(String thread_name) {
		this.thread_name = thread_name;
	}
	public String getTask_status() {
		return task_status;
	}
	public void setTask_status(String task_status) {
		this.task_status = task_status;
	}
	public String getTask_start_time() {
		return task_start_time;
	}
	public void setTask_start_time(String task_start_time) {
		this.task_start_time = task_start_time;
	}
	public String getTask_end_time() {
		return task_end_time;
	}
	public void setTask_end_time(String task_end_time) {
		this.task_end_time = task_end_time;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public long getVerify_count() {
		return verify_count;
	}
	public void setVerify_count(long verify_count) {
		this.verify_count = verify_count;
	}
	public String getTask_error_msg() {
		return task_error_msg;
	}
	public void setTask_error_msg(String task_error_msg) {
		this.task_error_msg = task_error_msg;
	}
	public String getVerifyIds() {
		return verifyIds;
	}
	public void setVerifyIds(String verifyIds) {
		this.verifyIds = verifyIds;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 
	 
	 
}
