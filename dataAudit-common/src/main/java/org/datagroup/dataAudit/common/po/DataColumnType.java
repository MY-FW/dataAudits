package org.datagroup.dataAudit.common.po;

import java.io.Serializable;

/**
 * 【用于初始化表字段数据类型实体】
 * 
 * @author yannannan
 * @createTime 2018年12月3日
 * @updateTime 2018年12月3日
 * @desc
 */
public class DataColumnType implements Serializable{
	private static final long serialVersionUID = 1L;
	private String table_schema;//所属数据库
	private String table_name;//表名称
	private String column_name;//字段名称
	private String data_type;//字段类型
	
	
	public DataColumnType() {
		super();
	}
	public DataColumnType(String table_schema, String table_name,
			String column_name, String data_type) {
		super();
		this.table_schema = table_schema;
		this.table_name = table_name;
		this.column_name = column_name;
		this.data_type = data_type;
	}
	public String getTable_schema() {
		return table_schema;
	}
	public void setTable_schema(String table_schema) {
		this.table_schema = table_schema;
	}
	public String getTable_name() {
		return table_name;
	}
	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public String getData_type() {
		return data_type;
	}
	public void setData_type(String data_type) {
		this.data_type = data_type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
