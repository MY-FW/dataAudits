package org.datagroup.dataAudit.common.po;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class SysUserInfo implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int id;
  private String account;
  private String userName;
  private String password;
  private String company;
  private String phone;
  private String emil;
  private String createTime;
  private String status;
  private String remarks;
  private Set<SysRoleInfo> roleInfos = new HashSet<>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmil() {
    return emil;
  }

  public void setEmil(String emil) {
    this.emil = emil;
  }

  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public Set<SysRoleInfo> getRoleInfos() {
    return roleInfos;
  }

  public void setRoleInfos(Set<SysRoleInfo> roleInfos) {
    this.roleInfos = roleInfos;
  }

}
