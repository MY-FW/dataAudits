package org.datagroup.dataAudit.common.po;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class SysRoleInfo implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int id;
  private String roleName;
  private String roleDescribe;
  private String roleCode;
  private String status;
  private String remarks;
  private Set<SysUserInfo> userInfos = new HashSet<>();
  private Set<SysMenuInfo> menuInfos = new HashSet<>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleDescribe() {
    return roleDescribe;
  }

  public void setRoleDescribe(String roleDescribe) {
    this.roleDescribe = roleDescribe;
  }

  public String getRoleCode() {
    return roleCode;
  }

  public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public Set<SysUserInfo> getUserInfos() {
    return userInfos;
  }

  public void setUserInfos(Set<SysUserInfo> userInfos) {
    this.userInfos = userInfos;
  }

  public Set<SysMenuInfo> getMenuInfos() {
    return menuInfos;
  }

  public void setMenuInfos(Set<SysMenuInfo> menuInfos) {
    this.menuInfos = menuInfos;
  }

}
