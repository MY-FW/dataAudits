package org.datagroup.dataAudit.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import org.datagroup.dataAudit.common.po.DataColumnType;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyTask;

public class VerifyDataCache {
	public static ExecutorService exe=null;
	
	/**
	 * 【线程状态缓存】
	 * @param key=threadName,value=status
	 */
	public static Map<String,Boolean> THREAD_STATUS_CACHE=new ConcurrentHashMap<String,Boolean>();
	
	/**
	 * 【任务缓存】
	 */
	public static Map<String,TbVerifyTask> TASK_CACHE=new ConcurrentHashMap<String,TbVerifyTask>();
	
	
	/**
	 * 【存储表数据字段类型】
	 */
	public static Map<String,ConcurrentHashMap<String,String>> TABLE_COLUMN_TYPE=new ConcurrentHashMap<String, ConcurrentHashMap<String,String>>();
	
	public static void putColumnType(DataColumnType dataColumnType){
			String tableNameAndSchema=dataColumnType.getTable_schema().trim().toUpperCase()+"#"+dataColumnType.getTable_name().trim().toUpperCase();
			String column=dataColumnType.getColumn_name().trim().toUpperCase();
			String columnDataType=dataColumnType.getData_type().trim().toUpperCase();
		if(TABLE_COLUMN_TYPE.containsKey(tableNameAndSchema)){
			TABLE_COLUMN_TYPE.get(tableNameAndSchema).put(column, columnDataType);
		}else{
			ConcurrentHashMap<String,String> columnMap=new ConcurrentHashMap<String,String>();
			   columnMap.put(column, columnDataType);
			   TABLE_COLUMN_TYPE.put(tableNameAndSchema, columnMap);
		}
		
	}
	
	
	/**
	 * 【稽核表配置信息初始化】
	 */
	public static Map<Integer,TbVerifyConfig> VERIFY_CONFIG_CACHE=new ConcurrentHashMap<Integer,TbVerifyConfig>();
	
	
	//***********************版本2**************************
	/**
	 * kay=sourceTable
	 * mapKey=targetTable
	 */
	public static Map<String, ConcurrentHashMap<String, TbVerifyConfig>> VERIFY_CONFIG_CACHE2=new ConcurrentHashMap<String,ConcurrentHashMap<String,TbVerifyConfig>>();
	public static void putVerifyConfigCache(TbVerifyConfig tbVerifyConfig){
		String sourceTable=tbVerifyConfig.getSource_table().trim().toUpperCase();
		if(VERIFY_CONFIG_CACHE2.containsKey(sourceTable)){//如果已存在
			ConcurrentHashMap<String, TbVerifyConfig> itemMap=VERIFY_CONFIG_CACHE2.get(tbVerifyConfig.getSource_table());//源表数据
			itemMap.put(tbVerifyConfig.getTarget_table(), tbVerifyConfig);
			VERIFY_CONFIG_CACHE2.put(sourceTable, itemMap);
		}else{
			ConcurrentHashMap<String, TbVerifyConfig> itemMap=new ConcurrentHashMap<String,TbVerifyConfig>();
			itemMap.put(tbVerifyConfig.getTarget_table(), tbVerifyConfig);
			VERIFY_CONFIG_CACHE2.put(sourceTable,itemMap);
		}
	}
	
}
