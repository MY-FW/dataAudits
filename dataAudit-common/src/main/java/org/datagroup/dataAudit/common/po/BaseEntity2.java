package org.datagroup.dataAudit.common.po;

import java.io.Serializable;

public class BaseEntity2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Integer id;

	private Integer pageIndex = Integer.valueOf(0); // 起始页索引

	protected Integer limit = 10; // 每页大小

	protected Integer currentPageIndex;

	protected Integer page = 1; // 当前页

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getCurrentPageIndex() {
		return (getPage()-1)*getLimit();
	}

	public void setCurrentPageIndex(Integer currentPageIndex) {
		this.currentPageIndex = (getPage()-1)*getLimit();
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	

}
