package org.datagroup.dataAudit.common.basepo;

import java.io.Serializable;

import org.datagroup.dataAudit.common.exception.ResponseEnum;
import org.datagroup.dataAudit.common.util.Strings;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 响应数据封装
 * 
 * @author Ivan
 *
 */
public class ResponseInfo<T> implements Serializable {

  private static final long serialVersionUID = -4112845183658483869L;

  /** 响应状态码 **/
  private String code;

  /** 响应内容 **/
  private String msg;

  /** 响应实体 **/
  private T data;

  public ResponseInfo() {
  }

  public ResponseInfo(String code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  public ResponseInfo(String code, String msg, T data) {
    this.code = code;
    this.msg = msg;
    this.data = data;
  }

  public ResponseInfo(ResponseEnum responseEnum, T data) {
    this.code = responseEnum.getCode();
    this.msg = Strings.format(responseEnum.getDesc());
    this.data = data;
  }

  public ResponseInfo(ResponseEnum responseEnum, Object... arguments) {
    this.code = responseEnum.getCode();
    this.msg = Strings.format(responseEnum.getDesc(), arguments);
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  /*
   * , Cloneable
   * 
   * @Override public Object clone() { try { return super.clone(); } catch
   * (CloneNotSupportedException e) { return null; } }
   */

  public String toJSONText() {
    if (this.data == null) {
      data = (T) new JSONObject();
    }
    return JSON.toJSONString(this);
  }
}
