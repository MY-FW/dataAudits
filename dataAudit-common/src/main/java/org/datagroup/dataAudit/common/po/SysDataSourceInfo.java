package org.datagroup.dataAudit.common.po;

import java.io.Serializable;

public class SysDataSourceInfo implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String id;
  private String base_code;
  private String base_cn_name;
  private String base_type;
  private String base_name;
  private String base_url;
  private String base_ip;
  private String user_name;
  private String password;
  private String initia_size;
  private String base_port;
  private String base_dcn;
  private String create_time;
  private String modify_time;
  private String status;
  private String remarks;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBase_code() {
    return base_code;
  }

  public void setBase_code(String base_code) {
    this.base_code = base_code;
  }

  public String getBase_type() {
    return base_type;
  }

  public void setBase_type(String base_type) {
    this.base_type = base_type;
  }

  public String getBase_name() {
    return base_name;
  }

  public void setBase_name(String base_name) {
    this.base_name = base_name;
  }

  public String getBase_url() {
    return base_url;
  }

  public void setBase_url(String base_url) {
    this.base_url = base_url;
  }

  public String getUser_name() {
    return user_name;
  }

  public void setUser_name(String user_name) {
    this.user_name = user_name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getInitia_size() {
    return initia_size;
  }

  public void setInitia_size(String initia_size) {
    this.initia_size = initia_size;
  }

  public String getBase_cn_name() {
    return base_cn_name;
  }

  public void setBase_cn_name(String base_cn_name) {
    this.base_cn_name = base_cn_name;
  }

  public String getBase_port() {
    return base_port;
  }

  public void setBase_port(String base_port) {
    this.base_port = base_port;
  }

  public String getBase_dcn() {
    return base_dcn;
  }

  public void setBase_dcn(String base_dcn) {
    this.base_dcn = base_dcn;
  }

  public String getCreate_time() {
    return create_time;
  }

  public void setCreate_time(String create_time) {
    this.create_time = create_time;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public String getBase_ip() {
    return base_ip;
  }

  public void setBase_ip(String base_ip) {
    this.base_ip = base_ip;
  }

  public String getModify_time() {
    return modify_time;
  }

  public void setModify_time(String modify_time) {
    this.modify_time = modify_time;
  }

}
