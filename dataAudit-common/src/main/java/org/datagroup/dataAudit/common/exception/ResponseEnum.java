package org.datagroup.dataAudit.common.exception;

/**
 * 返回响应状态枚举
 * 
 * @author Ivan
 *
 */
public enum ResponseEnum {
  /** 成功 **/
  SYS_SUCCESS("0000", "操作成功!"),
  /** 失败 **/
  SYS_FAILD("0001", "操作失败!"),
  /** 失败 **/
  SYS_FAILD_IUD("0011", "操作失败，请确认输入是否正确！"),
  /** 无数据 **/
  SYS_DATANUll("0002", "无满足条件的数据!"),
  /** 未知 **/
  ERROR_MSG("1000", "未知错误!"),
  /** 请求参数不正确 **/
  ERROR_PARAM_VALID("1002", "入参错误!"),
  /** 请求参数为空 **/
  ERROR_PARAM_NULL("1003", "入参为空!"),
  /** JSON格式不正确 **/
  ERROR_VALIDATE_JSON_FORMAT("1004", "JSON格式有误!{0}"),
  /** 请求超时 **/
  REQUEST_TIMEOUT("1005", "请求超时! {0}"),
  /** 解密失败 **/
  DECRYPT_FAIL("1006", "解密失败! {0}"),
  /** 签名校验不通过 **/
  SIGN_FAIL("1007", "签名校验不通过! {0}"),
  /** 没有配置IP白名单 **/
  GRANT_IP_NOT_EXISTS("1008", "系统没有配置IP白名单!"),
  /** 请求IP没有授权 **/
  REQUEST_IP_NOT_GRANT("1009", "请求IP：{0} 未授权!"),
  /** 数据源配置 **/
  DS_EXIST("1010", "数据源已存在!");

  private final String code;
  private final String desc;

  private ResponseEnum(String code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  public String getCode() {
    return code;
  }

  public String getDesc() {
    return desc;
  }
}
