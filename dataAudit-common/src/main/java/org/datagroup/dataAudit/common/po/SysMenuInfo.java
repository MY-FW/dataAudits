package org.datagroup.dataAudit.common.po;

import java.io.Serializable;
import java.util.Set;

public class SysMenuInfo implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int id;
  private String menuLvl;
  private String menuName;
  private String menuPath;
  private String menuCode;
  private String menuOder;
  private String menuFatherCode;
  private String createTime;
  private String status;
  private String remarks;
  private Set<SysRoleInfo> roles;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMenuLvl() {
    return menuLvl;
  }

  public void setMenuLvl(String menuLvl) {
    this.menuLvl = menuLvl;
  }

  public String getMenuName() {
    return menuName;
  }

  public void setMenuName(String menuName) {
    this.menuName = menuName;
  }

  public String getMenuPath() {
    return menuPath;
  }

  public void setMenuPath(String menuPath) {
    this.menuPath = menuPath;
  }

  public String getMenuCode() {
    return menuCode;
  }

  public void setMenuCode(String menuCode) {
    this.menuCode = menuCode;
  }

  public String getMenuOder() {
    return menuOder;
  }

  public void setMenuOder(String menuOder) {
    this.menuOder = menuOder;
  }

  public String getMenuFatherCode() {
    return menuFatherCode;
  }

  public void setMenuFatherCode(String menuFatherCode) {
    this.menuFatherCode = menuFatherCode;
  }

  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public Set<SysRoleInfo> getRoles() {
    return roles;
  }

  public void setRoles(Set<SysRoleInfo> roles) {
    this.roles = roles;
  }

}
