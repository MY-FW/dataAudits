package org.datagroup.dataAudit.common.po;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 
 * @author yannannan
 * @createTime 2018年11月29日
 * @updateTime 2018年11月29日
 * @desc 稽核配置实体类
 */
public class TbVerifyConfig extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer verify_id;
  private String source_inc_table;// 配置项增量ogg表名
  private String inc_table_count;// 原表数据总量
  private String source_base_code;// 配置项增量ogg字段名
  private String target_base_code;// 源目标表名
  private String source_base_name;// 数据库名字
  private String target_base_name;// 目标数据库名字
  private String source_base_en_name;// 源目标表字段
  private String target_base_en_name;// 存储过程名称
  private String source_table;// 稽核开始时间
  private String target_table;// 稽核结束时间
  private String target_sk_field;//分片建字段
  private String process_name;// 增量配置稽核状态【状态：70A待开始，70B稽核中，70C稽核完成，70E稽核异常】
  private String source_fields;// 执行开始时间
  private String target_fields;// 执行结束时间
  private String create_time;// 创建时间
  private String start_time;
  private String end_time;
  private String audit_status;// 稽核状态 JHZ：稽核中；WJH：未稽核
  private String status;// 状态 KY：可用；JY：禁用
  private Integer center_type;// 所属中心例如：1cpc、2、客户管理能力中心
  private String remarks;// 备注说明
  private String verify_start_time;
  private String verify_end_time;
  private String verifyIds;// 逗号隔开的verify_id
  
public TbVerifyConfig() {
	super();
}
public TbVerifyConfig(Integer verify_id, String source_inc_table,
		String inc_table_count, String source_base_code,
		String target_base_code, String source_base_name,
		String target_base_name, String source_base_en_name,
		String target_base_en_name, String source_table, String target_table,
		String target_sk_field, String process_name, String source_fields,
		String target_fields, String create_time, String start_time,
		String end_time, String audit_status, String status,
		Integer center_type, String remarks, String verify_start_time,
		String verify_end_time, String verifyIds) {
	super();
	this.verify_id = verify_id;
	this.source_inc_table = source_inc_table;
	this.inc_table_count = inc_table_count;
	this.source_base_code = source_base_code;
	this.target_base_code = target_base_code;
	this.source_base_name = source_base_name;
	this.target_base_name = target_base_name;
	this.source_base_en_name = source_base_en_name;
	this.target_base_en_name = target_base_en_name;
	this.source_table = source_table;
	this.target_table = target_table;
	this.target_sk_field = target_sk_field;
	this.process_name = process_name;
	this.source_fields = source_fields;
	this.target_fields = target_fields;
	this.create_time = create_time;
	this.start_time = start_time;
	this.end_time = end_time;
	this.audit_status = audit_status;
	this.status = status;
	this.center_type = center_type;
	this.remarks = remarks;
	this.verify_start_time = verify_start_time;
	this.verify_end_time = verify_end_time;
	this.verifyIds = verifyIds;
}
public Integer getVerify_id() {
	return verify_id;
}
public void setVerify_id(Integer verify_id) {
	this.verify_id = verify_id;
}
public String getSource_inc_table() {
	return source_inc_table;
}
public void setSource_inc_table(String source_inc_table) {
	this.source_inc_table = source_inc_table;
}
public String getInc_table_count() {
	return inc_table_count;
}
public void setInc_table_count(String inc_table_count) {
	this.inc_table_count = inc_table_count;
}
public String getSource_base_code() {
	return source_base_code;
}
public void setSource_base_code(String source_base_code) {
	this.source_base_code = source_base_code;
}
public String getTarget_base_code() {
	return target_base_code;
}
public void setTarget_base_code(String target_base_code) {
	this.target_base_code = target_base_code;
}
public String getSource_base_name() {
	return source_base_name;
}
public void setSource_base_name(String source_base_name) {
	this.source_base_name = source_base_name;
}
public String getTarget_base_name() {
	return target_base_name;
}
public void setTarget_base_name(String target_base_name) {
	this.target_base_name = target_base_name;
}
public String getSource_base_en_name() {
	return source_base_en_name;
}
public void setSource_base_en_name(String source_base_en_name) {
	this.source_base_en_name = source_base_en_name;
}
public String getTarget_base_en_name() {
	return target_base_en_name;
}
public void setTarget_base_en_name(String target_base_en_name) {
	this.target_base_en_name = target_base_en_name;
}
public String getSource_table() {
	return source_table;
}
public void setSource_table(String source_table) {
	this.source_table = source_table;
}
public String getTarget_table() {
	return target_table;
}
public void setTarget_table(String target_table) {
	this.target_table = target_table;
}
public String getTarget_sk_field() {
	return target_sk_field;
}
public void setTarget_sk_field(String target_sk_field) {
	this.target_sk_field = target_sk_field;
}
public String getProcess_name() {
	return process_name;
}
public void setProcess_name(String process_name) {
	this.process_name = process_name;
}
public String getSource_fields() {
	return source_fields;
}
public void setSource_fields(String source_fields) {
	this.source_fields = source_fields;
}
public String getTarget_fields() {
	return target_fields;
}
public void setTarget_fields(String target_fields) {
	this.target_fields = target_fields;
}
public String getCreate_time() {
	return create_time;
}
public void setCreate_time(String create_time) {
	this.create_time = create_time;
}
public String getStart_time() {
	return start_time;
}
public void setStart_time(String start_time) {
	this.start_time = start_time;
}
public String getEnd_time() {
	return end_time;
}
public void setEnd_time(String end_time) {
	this.end_time = end_time;
}
public String getAudit_status() {
	return audit_status;
}
public void setAudit_status(String audit_status) {
	this.audit_status = audit_status;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Integer getCenter_type() {
	return center_type;
}
public void setCenter_type(Integer center_type) {
	this.center_type = center_type;
}
public String getRemarks() {
	return remarks;
}
public void setRemarks(String remarks) {
	this.remarks = remarks;
}
public String getVerify_start_time() {
	return verify_start_time;
}
public void setVerify_start_time(String verify_start_time) {
	this.verify_start_time = verify_start_time;
}
public String getVerify_end_time() {
	return verify_end_time;
}
public void setVerify_end_time(String verify_end_time) {
	this.verify_end_time = verify_end_time;
}
public String getVerifyIds() {
	return verifyIds;
}
public void setVerifyIds(String verifyIds) {
	this.verifyIds = verifyIds;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}

}
