package org.datagroup.dataAudit.common.po;

import java.io.Serializable;

public class TbVerifyRepairTask extends BaseEntity2 implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer task_id;//修复推送记录任务标识
	private String thread_name;//修复推送线程名称
	private String repair_start_time;//修复推送状态
	private String repair_end_time;//修复推送结束时间
	private String source_table;//原表名称
	private String target_table;//目标表名称
	private Integer verify_type;//修复原因类型=稽核结果类型【0数据一致！、1值不匹配！、2源表字段空值！，目标字段有值、3源字段有值，目标表字段空值！、4目标数据不存在！、5条件不匹配，无需转换!，6源数据已删除，目标数据已存在，数据不一直!、7稽核异常】
	private String verify_desc;//修复原因类型描述=结婚结果类型描述【0数据一致！、1值不匹配！、2源表字段空值！，目标字段有值、3源字段有值，目标表字段空值！、4目标数据不存在！、5条件不匹配，无需转换!，6源数据已删除，目标数据已存在，数据不一直!、7稽核异常】
	private String push_status;//线程推送状态TSZ推送中、TSF推送完成、TSE推送异常
	private long push_count;//推送条数
	private String push_error_msg;//推送异常错误描述s
	
	
	public TbVerifyRepairTask() {
		super();
	}
	public TbVerifyRepairTask(Integer task_id, String thread_name,
			String repair_start_time, String repair_end_time,
			String source_table, String target_table, Integer verify_type,
			String verify_desc, String push_status, long push_count,
			String push_error_msg) {
		super();
		this.task_id = task_id;
		this.thread_name = thread_name;
		this.repair_start_time = repair_start_time;
		this.repair_end_time = repair_end_time;
		this.source_table = source_table;
		this.target_table = target_table;
		this.verify_type = verify_type;
		this.verify_desc = verify_desc;
		this.push_status = push_status;
		this.push_count = push_count;
		this.push_error_msg = push_error_msg;
	}
	public Integer getTask_id() {
		return task_id;
	}
	public void setTask_id(Integer task_id) {
		this.task_id = task_id;
	}
	public String getThread_name() {
		return thread_name;
	}
	public void setThread_name(String thread_name) {
		this.thread_name = thread_name;
	}
	public String getRepair_start_time() {
		return repair_start_time;
	}
	public void setRepair_start_time(String repair_start_time) {
		this.repair_start_time = repair_start_time;
	}
	public String getRepair_end_time() {
		return repair_end_time;
	}
	public void setRepair_end_time(String repair_end_time) {
		this.repair_end_time = repair_end_time;
	}
	public String getSource_table() {
		return source_table;
	}
	public void setSource_table(String source_table) {
		this.source_table = source_table;
	}
	public String getTarget_table() {
		return target_table;
	}
	public void setTarget_table(String target_table) {
		this.target_table = target_table;
	}
	public Integer getVerify_type() {
		return verify_type;
	}
	public void setVerify_type(Integer verify_type) {
		this.verify_type = verify_type;
	}
	public String getVerify_desc() {
		return verify_desc;
	}
	public void setVerify_desc(String verify_desc) {
		this.verify_desc = verify_desc;
	}
	public String getPush_status() {
		return push_status;
	}
	public void setPush_status(String push_status) {
		this.push_status = push_status;
	}
	public long getPush_count() {
		return push_count;
	}
	public void setPush_count(long push_count) {
		this.push_count = push_count;
	}
	public String getPush_error_msg() {
		return push_error_msg;
	}
	public void setPush_error_msg(String push_error_msg) {
		this.push_error_msg = push_error_msg;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
