package org.datagroup.dataAudit.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap; 
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataAudit.common.po.SiebelIncPojo;
import org.datagroup.dataAudit.common.po.TbVerifyConfig;
import org.datagroup.dataAudit.common.po.TbVerifyLog;


public class VerifyDataModule { 
	private static final Logger log = LogManager.getLogger(VerifyDataModule.class);
	
	/**
	 * 1、【判断源数据是否存在】
	 * 2、【判断目标数据是否存在】
	 */
	public TbVerifyLog validateCallExist(TbVerifyConfig verifyConfig,String callStr,List<Map<String,Object>> mysqlVerifyValueList,SiebelIncPojo siebelIncPojo){
		TbVerifyLog verifyLog=new TbVerifyLog();
		verifyLog.setSource_base_code(verifyConfig.getSource_base_code());
		verifyLog.setTarget_base_code(verifyConfig.getTarget_base_code());
		verifyLog.setSource_base_name(verifyConfig.getSource_base_name());
		verifyLog.setTarget_base_name(verifyConfig.getTarget_base_name());
		verifyLog.setSource_inc_table(verifyConfig.getSource_inc_table());
		verifyLog.setRepair_status(0);//数据修复推送状态(0、未转移、1、已转移)
		verifyLog.setSource_table(verifyConfig.getSource_table());//源表
		verifyLog.setTarget_table(verifyConfig.getTarget_table());//目标表
		verifyLog.setRow_id(siebelIncPojo.getTable_row_id());//稽核数据id
		verifyLog.setInc_row_id(siebelIncPojo.getRow_id());//inc表数据主键
		verifyLog.setCenter_type(verifyConfig.getCenter_type());
		verifyLog.setRepair_status(0);//数据修复推送状态(0、未转移、1、已转移)
		verifyLog.setOp_type(getValidateOpType(siebelIncPojo.getOp_type()));
		verifyLog.setVerify_status("70C");//已稽核
		try{	
			if(callStr==null||callStr.equals("")){
				//条件不匹配，无需转换!
				verifyLog.setVerify_type(5);//条件不匹配，无需转换!//稽核结果类型稽核结果类型【0数据一致！、1值不匹配！、2源表字段空值！，目标字段有值、3源字段有值，目标表字段空值！、4目标数据不存在！、5条件不匹配，无需转换!，6源数据已删除，目标数据已存在，数据不一直!、7稽核异常】
				verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
				SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
				verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
				verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
				verifyLog.setVerify_cd("N");//稽核失败
				return verifyLog;
			}
			if(mysqlVerifyValueList==null||mysqlVerifyValueList.size()==0){
					//目标数据不存在
					verifyLog.setVerify_type(4);//目标数据不存在
					verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
					SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
					verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
					verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
					verifyLog.setVerify_cd("N");//稽核失败
					return verifyLog;
			}
			return null;
		}catch(Exception e){
			log.info("validateCallExist异常参数表参数1：【"+verifyConfig.getSource_table()+"】【"+verifyConfig.getTarget_table()+"】【"+siebelIncPojo.getRow_id()+"#"+siebelIncPojo.getTable_row_id()+"】");
			log.info("validateCallExist异常存储过程2："+callStr);
			log.info("validateCallExist稽核异常："+e.getMessage(),e);
			verifyLog.setVerify_status("70E");
			verifyLog.setVerify_type(7);//稽核异常
			verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
			verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
			verifyLog.setVerify_cd("N");//稽核失败
			e.printStackTrace();
			return verifyLog;
		}
	}
	
	
	
	/**
	 * 【判定源表字段空值，目标字段有值情况、及稽核比对情况】
	 * @param verifyType
	 * @return
	 */ 
	public void validateSrcFieldValueExist(TbVerifyConfig verifyConfig,SiebelIncPojo siebelIncPojo,String[] srcItemValue,List<Map<String,Object>> targetMysqlValueList,List<TbVerifyLog> verifyLogList){
		boolean specialStatus=false;
		if(srcItemValue[0].indexOf("@")!=-1&&srcItemValue[0].lastIndexOf("@")!=-1){
			specialStatus=true;
		}
		
					TbVerifyLog verifyLog=new TbVerifyLog();
					verifyLog.setSource_base_code(verifyConfig.getSource_base_code());
					verifyLog.setTarget_base_code(verifyConfig.getTarget_base_code());
					verifyLog.setSource_base_name(verifyConfig.getSource_base_name());
					verifyLog.setTarget_base_name(verifyConfig.getTarget_base_name());
					verifyLog.setSource_inc_table(verifyConfig.getSource_inc_table());
					verifyLog.setRepair_status(0);//数据修复推送状态(0、未转移、1、已转移)
					verifyLog.setSource_table(verifyConfig.getSource_table());//源表
					verifyLog.setTarget_table(verifyConfig.getTarget_table());//目标表
					verifyLog.setRow_id(siebelIncPojo.getTable_row_id());//稽核数据id
					verifyLog.setInc_row_id(siebelIncPojo.getRow_id());//inc表数据主键
					verifyLog.setCenter_type(verifyConfig.getCenter_type());
					verifyLog.setRepair_status(0);//数据修复推送状态(0、未转移、1、已转移)
					verifyLog.setOp_type(getValidateOpType(siebelIncPojo.getOp_type()));
					verifyLog.setVerify_status("70C");//已稽核
		try{		
					int specialLen=0;//用于区分特殊是否有值0非特殊、1特殊无值、2特殊有值
					if(specialStatus==true){
						String[] specialValue=srcItemValue[1].split("@");
						specialLen=specialValue.length;
					}
					
					if(srcItemValue.length==1||(specialStatus==true&&(specialLen==0||specialLen==1))){//源字段无值或者特殊字段也无值
						String srcFtargetF=srcItemValue[0].trim().toUpperCase();//srcField#targetField
						String[] fieldArr=srcFtargetF.split("#");
						boolean srcFTargetStatus=false;
						for(int i=0;i<targetMysqlValueList.size();i++){
							 Map<String,Object> newRefMysqlValue=null;
							 if(specialStatus==true){
								 Map<String,String> specialMap=new HashMap<String, String>();
								 String[] specialArray=fieldArr[1].trim().split("@");
								 specialMap.put(specialArray[0].trim(),fieldArr[1].trim());
								 newRefMysqlValue=setSpecialMysqlValue(specialMap, targetMysqlValueList.get(i));//特殊处理
								 Object objectValue=newRefMysqlValue.get(fieldArr[1].trim());//得到源字段值
								 if(objectValue.toString().trim().equals(srcItemValue[1].trim())){//特殊情况2者都无值正常一致
									 srcFTargetStatus=false;
									 break;
								 }else{//都不相等
									 String[] speciId=objectValue.toString().trim().split("@");//mysql值
									 if(speciId[0].trim().equals(srcItemValue[1].split("@")[0])&&speciId.length>1){
										 srcFTargetStatus=true;
									 		break;
									 }else{
										 continue;
									 }
								 }
								 
								 
							 }else{
								newRefMysqlValue=targetMysqlValueList.get(i);
								Object objectValue=newRefMysqlValue.get(fieldArr[1].trim());//得到源字段值
								 if(objectValue!=null&&!objectValue.toString().trim().equals("")){
								 		//源字段无值，目标数据有值
								 		srcFTargetStatus=true;
								 		break;
								 }
							 }
						}
						if(srcFTargetStatus==true){
							//源表字段空值！，目标字段有值
							verifyLog.setVerify_type(2);//源表字段空值！，目标字段有值
							verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
							SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
							verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
							verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
							verifyLog.setVerify_cd("N");//稽核失败
							verifyLog.setSource_column(fieldArr[0]);//源字段名称
							verifyLog.setTarget_column(fieldArr[1].trim());//目标字段名称
							if(targetMysqlValueList.size()==1){//如果目标就1条数据记录一下目标字段值
								verifyLog.setTarget_value(targetMysqlValueList.get(0).get(fieldArr[1].trim()).toString());
							}
							verifyLogList.add(verifyLog);
						}else{
							//源表字段空值！，目标字段也空值数据一致
							verifyLog.setVerify_type(0);//源表字段空值！，目标字段有值
							verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
							SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
							verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
							verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
							verifyLog.setVerify_cd("Y");//稽核成功
							verifyLog.setSource_column(fieldArr[0]);//源字段名称
							verifyLog.setTarget_column(fieldArr[1].trim());//目标字段名称
							verifyLogList.add(verifyLog);
						}
							
					}else if(srcItemValue.length>1){
								//源数据有值情况,目标也有值开始稽核
								String srcFtargetF=srcItemValue[0].trim().toUpperCase();//srcField#targetField
								String[] fieldArr=srcFtargetF.split("#");
								String srcValue=srcItemValue[1].trim();
								boolean verifyStatus=false;
								String targetValue="";
								boolean targetValueStatus=false;//目标字段有值
								boolean yesSpecValue=false;
								for(int i=0;i<targetMysqlValueList.size();i++){
									 Map<String,Object> newRefMysqlValue=null;
									 if(specialStatus==true){
										 Map<String,String> specialMap=new HashMap<String, String>();
										 String[] specialArray=fieldArr[1].trim().split("@");
										 specialMap.put(specialArray[0].trim(),fieldArr[1].trim());
										 newRefMysqlValue=setSpecialMysqlValue(specialMap, targetMysqlValueList.get(i));//特殊处理
									 }else{
										newRefMysqlValue=targetMysqlValueList.get(i);
									 }
									 Object objectValue=newRefMysqlValue.get(fieldArr[1].trim());//得到源字段值
									 	if(objectValue!=null&&!objectValue.toString().trim().equals("")){
									 		String mysqlValue=objectValue.toString().trim();
									 		if(mysqlValue.lastIndexOf(".")!=-1&&mysqlValue.indexOf("-")!=-1&&mysqlValue.lastIndexOf(":")!=-1){
									 			mysqlValue=mysqlValue.substring(0,mysqlValue.length()-2);
//									 			log.info("处理后：mysqlValue="+mysqlValue+",srcValue="+srcValue);
									 		}
									 		if(mysqlValue.equals(srcValue)){
									 			verifyStatus=true;
									 			targetValueStatus=false;
									 			targetValue=mysqlValue;
									 			break;
									 		}else{//全部不一直要区分特别情况
									 			if(specialStatus==true){
										 			String mysqlSpecValue=mysqlValue.split("@")[0].trim();
										 			if(!srcValue.contains(mysqlSpecValue)){//仅处理特殊情况并且不包含mysql值
										 				continue;
										 			}else{//mysql中包含但是值不匹配
										 				targetValue=mysqlValue;
										 				yesSpecValue=true;
										 			}
									 			}
									 		}
									 	}else{
									 		targetValueStatus=true;//目标字段无值
									 	}
						}
						if(verifyStatus==true){//稽核成功
							verifyLog.setVerify_type(0);//源表字段有值！，目标字段有值
							verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
							SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
							verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
							verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
							verifyLog.setVerify_cd("Y");//稽核成功
							verifyLog.setSource_column(fieldArr[0]);//源字段名称
							verifyLog.setSource_value(srcValue);
							verifyLog.setTarget_column(fieldArr[1].trim());//目标字段名称
							verifyLog.setTarget_value(targetValue);
							verifyLogList.add(verifyLog);
						}else{
							if(targetValueStatus==true&&srcValue!=null&&!srcValue.equals("")||(yesSpecValue==false&&targetValueStatus==true)){
								verifyLog.setVerify_type(3);//源表字段有值！，目标字段无值
							}else{
								verifyLog.setVerify_type(1);//源表字段有值！，目标字段有值值不匹配
							}
							
							
							//值不匹配
							
							verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
							SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
							verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
							verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
							verifyLog.setVerify_cd("N");//稽核失败
							verifyLog.setSource_column(fieldArr[0]);//源字段名称
							verifyLog.setSource_value(srcValue);
							verifyLog.setTarget_column(fieldArr[1].trim());//目标字段名称
							if(targetMysqlValueList.size()==1){//如果目标就1条数据记录一下目标字段值
								if(specialStatus==false&&yesSpecValue==true){//源表字段有值，目标
									verifyLog.setTarget_value(targetMysqlValueList.get(0).get(fieldArr[1].trim()).toString());
								}
							}
							verifyLogList.add(verifyLog);
						}
						
					}
		}catch(Exception e){
			log.info("validateCallExist异常参数表参数1：【"+verifyConfig.getSource_table()+"】【"+verifyConfig.getTarget_table()+"】【"+siebelIncPojo.getRow_id()+"#"+siebelIncPojo.getTable_row_id()+"】");
			log.info("validateCallExist稽核异常："+e.getMessage(),e);
			verifyLog.setVerify_status("70E");
			verifyLog.setVerify_type(7);//稽核异常
			verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
			verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
			verifyLog.setVerify_cd("N");//稽核失败
			verifyLogList.add(verifyLog);
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 【效验delete删除模式】
	 * @param verifyType
	 * @return
	 */
	public void validateDeleteExist(TbVerifyConfig verifyConfig,SiebelIncPojo siebelIncPojo,int targetCount,List<TbVerifyLog> verifyLogList){
		TbVerifyLog verifyLog=new TbVerifyLog();
		verifyLog.setSource_base_code(verifyConfig.getSource_base_code());
		verifyLog.setTarget_base_code(verifyConfig.getTarget_base_code());
		verifyLog.setSource_base_name(verifyConfig.getSource_base_name());
		verifyLog.setTarget_base_name(verifyConfig.getTarget_base_name());
		verifyLog.setSource_inc_table(verifyConfig.getSource_inc_table());
		verifyLog.setRepair_status(0);//数据修复推送状态(0、未转移、1、已转移)
		verifyLog.setSource_table(verifyConfig.getSource_table());//源表
		verifyLog.setTarget_table(verifyConfig.getTarget_table());//目标表
		verifyLog.setRow_id(siebelIncPojo.getTable_row_id());//稽核数据id
		verifyLog.setInc_row_id(siebelIncPojo.getRow_id());//inc表数据主键
		verifyLog.setVerify_cd("70C");//稽核状态【70A待稽核、70B稽核中、70C稽核失败、70E稽核异常】
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
		verifyLog.setCreate_time(sdf.format(new Date(System.currentTimeMillis())));
		verifyLog.setLast_update(DateUtil.getDatePatternVerify(siebelIncPojo.getLast_upd(),"yyyyMMddhhmmss"));
		verifyLog.setCenter_type(verifyConfig.getCenter_type());
		verifyLog.setRepair_status(0);//数据修复推送状态(0、未转移、1、已转移)
		verifyLog.setOp_type(getValidateOpType(siebelIncPojo.getOp_type()));
		try{
			 if(targetCount>0){
				 //源数据已删除，目标数据已存在，数据不一直!
				 verifyLog.setVerify_type(6);//稽核结果类型稽核结果类型【0数据一致！、1值不匹配！、2源表字段空值！，目标字段有值、3源字段有值，目标表字段空值！、4目标数据不存在！、5条件不匹配，无需转换!，6源数据已删除，目标数据已存在，数据不一直!、7稽核异常】
				 verifyLog.setVerify_cd("N");//稽核结果【Y数据一直，N数据不一致】
			 }else{
				 verifyLog.setVerify_type(0);//稽核结果类型稽核结果类型【0数据一致！、1值不匹配！、2源表字段空值！，目标字段有值、3源字段有值，目标表字段空值！、4目标数据不存在！、5条件不匹配，无需转换!，6源数据已删除，目标数据已存在，数据不一直!、7稽核异常】
				 verifyLog.setVerify_cd("Y");
			 }
			 verifyLogList.add(verifyLog);
		}catch(Exception e){
			log.info("validateDeleteExist异常参数表参数1：【"+verifyConfig.getSource_table()+"】【"+verifyConfig.getTarget_table()+"】【"+siebelIncPojo.getRow_id()+"#"+siebelIncPojo.getTable_row_id()+"】");
			log.info("validateDeleteExist稽核异常："+e.getMessage(),e);
			verifyLog.setVerify_status("70E");
			verifyLog.setVerify_type(7);//稽核异常
			verifyLog.setVerify_desc(getVerifyDesc(verifyLog.getVerify_type()));//设置状态描述
			verifyLog.setVerify_cd("N");//稽核失败
			verifyLogList.add(verifyLog);
			e.printStackTrace();
		}
	}
	
	/**
	 * 【特殊处理】一条记录多个属性情况
	 * @param specialMap key=targetColumn,value=propertyField@valueField
	 * @param valueMap
	 * @return
	 */
	public Map<String,Object> setSpecialMysqlValue(Map<String,String> specialMap,Map<String,Object> valueMap){
		//特殊业务支持多属性
		if(specialMap!=null&&specialMap.size()>0){
			for(Map.Entry<String,String> specialItem:specialMap.entrySet()){
				//key=targetColumn,value=propertyField@valueField
				String specialStr=specialItem.getValue();//得到属性字段
				if(specialStr!=null&&!specialStr.equals("")){
					specialStr=specialStr.trim().toUpperCase();
				}
				String[] specialArray=specialStr.split("@");//key=targetColumn,value=propertyField@valueField
				String refValue=valueMap.get(specialArray[0])+"@"+valueMap.get(specialArray[1]);
				valueMap.put(specialStr,refValue);
			}
		}
		return valueMap;
	}
	
	
	
	
	/**
	 * 【稽核描述归类】
	 * @param verifyType
	 * @return
	 */
	public static String getVerifyDesc(int verifyType){
		String verifyDesc="稽核异常";
		try{
			switch (verifyType) {
			case 0:
				verifyDesc="数据一致！";
				break;
			case 1:
				verifyDesc="值不匹配！";
				break;
			case 2:
				verifyDesc="源表字段空值！，目标字段有值";
				break;
			case 3:
				verifyDesc="源字段有值，目标表字段空值！";
				break;
			case 4:
				verifyDesc="目标数据不存在！";
				break;
			case 5:
				verifyDesc="条件不匹配，无需转换!";
				break;
			case 6:
				verifyDesc="源数据已删除，目标数据已存在，数据不一直!";
				break;
			default:
				verifyDesc="稽核异常";
				break;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			verifyDesc="稽核异常";
		}
		return verifyDesc;
	}
	
	public String getValidateOpType(String opType){
		try{
			if(opType!=null&&!opType.equals("")){
				if(opType.trim().toUpperCase().contains("DELETE")){
					return "DELETE";
				}else if(opType.trim().toUpperCase().contains("UPDATE")){
					return "UPDATE";
				}else if(opType.trim().toUpperCase().contains("INSERT")){
					return "INSERT";
				}else{
					return opType;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info("操作类型无法识别!");
		}
		return opType;
	}
	
}
