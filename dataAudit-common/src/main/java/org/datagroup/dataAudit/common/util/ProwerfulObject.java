package org.datagroup.dataAudit.common.util;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.datagroup.dataAudit.common.po.TbVerifyTask;




public class ProwerfulObject {
	public static Map setSearchMap(Object object){ 
		try {	
				Map map=new HashMap();
				Class classs=object.getClass();
				Field[] fields=classs.getDeclaredFields();

				Method[] methods=classs.getDeclaredMethods();
				for(int i=0;i<fields.length;i++){
					for(int j=0;j<methods.length;j++){
						if(methods[j].getName().toLowerCase().indexOf(fields[i].getName().toLowerCase()) != -1 && methods[j].getName().indexOf("get")!=-1){
						
							if(methods[j].invoke(object)!=null){
								System.out.println("字段名称||"+fields[i].getName()+"\t\t方法名称||"+methods[j].getName()+"\t调用方法名称||\t"+methods[j].invoke(object));
								map.put(fields[i].getName(), methods[j].invoke(object));
								break;
							}
							
							
						}
						
						
					}
				}

				
				return map;
			
			}catch(Exception e){
				System.out.println("����Dao�㡾��ʼ�����󣡡�");
				e.printStackTrace();
			}
			
				
		
		return null;
	}
	
	
	/**
	 * 【反射实体类】封装属性
	 * @param Object：任意实体类  pkstatus:是否包含id主键[true包括，false 不包括]
	 * @return Field String and split [,]
	 * @author yannannan
	 */
	public static String getObjectFieldsString(Object object,boolean pkstatus){
		try{
			StringBuffer buffer=new StringBuffer();
						
		Class classs=object.getClass();
		Field[] fields=classs.getDeclaredFields();

		Method[] methods=classs.getDeclaredMethods();
		int ti=0;
		if(pkstatus==false){
			ti=1;
		}else{
			ti=0;
		}
		for(int i=ti;i<fields.length;i++){
						String fieldName=fields[i].getName();
						buffer.append(fieldName);
						buffer.append(",");
						//System.out.println(fields[i].getType().getSimpleName());
						
		}
		String fieldsSplitString=buffer.toString().substring(0,buffer.length()-1);
				return fieldsSplitString; 
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 【反射实体类】封装属性Value值
	 * @param Object：任意实体类
	 * @return Value Content String and split [,]
	 * @author yannannan
	 */
	public static List<Object> getObjectFieldsValue(Object object,boolean pkstatus){
		
		try{
			List<Object> entityValues=new ArrayList<Object>();
						
		Class classs=object.getClass();
		Field[] fields=classs.getDeclaredFields();

		Method[] methods=classs.getDeclaredMethods();
		int ti=0;
		if(pkstatus==false){
			ti=1;
		}else{
			ti=0;
		}
		for(int i=ti;i<fields.length;i++){
						String fieldName=fields[i].getName();
				PropertyDescriptor pd = new PropertyDescriptor(fieldName,classs);  
				            Method getMethod = pd.getReadMethod();//获得get方法  
				            Object setResultValue = getMethod.invoke(object);//执行get方法返回一个Object 
				            entityValues.add(setResultValue);
				            
		}
		
		return entityValues;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 【反射实体类】封装属性数据类型
	 * @param Object：任意实体
	 * @return Field String and split [,] 类数据类型
	 * @author yannannan
	 */
	public static List<String> getObjectFieldsSimpleType(Object object,boolean pkstatus){
		try{
			StringBuffer buffer=new StringBuffer();
						
		Class classs=object.getClass();
		Field[] fields=classs.getDeclaredFields();
		List<String> arrayList=new ArrayList<String>();
		Method[] methods=classs.getDeclaredMethods();
		int ti=0;
		if(pkstatus){
			ti=0;
		}else{
			ti=1;
		}
		for(int i=ti;i<fields.length;i++){
						String fieldName=fields[i].getName();
						buffer.append(fieldName);
						buffer.append(",");
						arrayList.add(fields[i].getType().getSimpleName());
		}
		return arrayList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 【反射实体类】获取数据库对象 【占位符?,?】
	 * @param Object：任意实体
	 * @return Field String and split [,] 类数据类型
	 * @author yannannan
	 */
	public static String getDatabaseSymbol(Object object,boolean pkstatus){
		String[] fieldList=getObjectFieldsString(object, pkstatus).split(",");
		StringBuffer buffer=new StringBuffer();
		for(int i=0;i<fieldList.length;i++){
			buffer.append(",?");
		}
		String symobl=buffer.toString().substring(1,buffer.toString().length());
		return symobl;
	}
	
	
	/**
	 * 【反射实体类】获取Myibatis 【占位符#{xxxx},#{XXXX}】
	 * @param Object：任意实体
	 * @param pkstatus 是否包含主键 true=包含 false=不包含
	 * @return Field String and split [,] #{xxx},#{xxxx}
	 * @author yannannan
	 */
	public static String getMyibatisSymbol(Object object,boolean pkstatus){
		try{
			StringBuffer buffer=new StringBuffer();
						
		Class classs=object.getClass();
		Field[] fields=classs.getDeclaredFields();

		Method[] methods=classs.getDeclaredMethods();
		int ti=0;
		if(pkstatus==false){
			ti=1;
		}else{
			ti=0;
		}
		for(int i=ti;i<fields.length;i++){
						String fieldName=fields[i].getName();
						buffer.append("#{"+fieldName+"}");
						buffer.append(",");
						//System.out.println(fields[i].getType().getSimpleName());
						
		}
		String fieldsSplitString=buffer.toString().substring(0,buffer.length()-1);
			return fieldsSplitString; 
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	


/**
 * 【自动创建表】
 * @param args
 */
public static boolean createDataBaseTable(String tableName,Object object,List<String> columnDescList){
	String[] tableColumnList=getObjectFieldsString(object,true).split(",");//包含主键
	List<String> thisDataTypeList=getObjectFieldsSimpleType(object,true);//得到数据类型
	PreparedStatement pstmt=null;
	Connection conn=null;
	try{
		StringBuffer tableColumnBuffer=new StringBuffer();
		for(int j=0;j<thisDataTypeList.size();j++){
			if(j==0){
				if(thisDataTypeList.get(j).equals("String")){//得到第一个字段类型
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(50) NOT NULL AUTO_INCREMENT");
				}else if(thisDataTypeList.get(j).equals("Long")||thisDataTypeList.get(j).equals("long")){
					tableColumnBuffer.append(","+tableColumnList[j]+" bigint(22) NOT NULL AUTO_INCREMENT");
				}else if(thisDataTypeList.get(j).equals("int")||thisDataTypeList.get(j).equals("Integer")){
					tableColumnBuffer.append(","+tableColumnList[j]+" bigint(22) NOT NULL AUTO_INCREMENT");
				}else if(thisDataTypeList.get(j).equals("float")||thisDataTypeList.get(j).equals("Float")){
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(50) NOT NULL AUTO_INCREMENT");
				}else if(thisDataTypeList.get(j).equals("short")||thisDataTypeList.get(j).equals("Short")){
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(22) NOT NULL AUTO_INCREMENT");
				}else if(thisDataTypeList.get(j).equals("double")||thisDataTypeList.get(j).equals("Double")){
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(22) NOT NULL AUTO_INCREMENT");
				}else if(thisDataTypeList.get(j).equals("byte[]")||thisDataTypeList.get(j).equals("Byte[]")){
					tableColumnBuffer.append(","+tableColumnList[j]+" mediumblob COMMENT '"+columnDescList.get(j)+"'");
				}
			}else{
				if(thisDataTypeList.get(j).equals("String")){//得到第一个字段类型
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(50) DEFAULT 0 COMMENT '"+columnDescList.get(j)+"'");
				}else if(thisDataTypeList.get(j).equals("Long")||thisDataTypeList.get(j).equals("long")){
					tableColumnBuffer.append(","+tableColumnList[j]+" bigint(22) DEFAULT 0 COMMENT '"+columnDescList.get(j)+"'");
				}else if(thisDataTypeList.get(j).equals("int")||thisDataTypeList.get(j).equals("Integer")){
					tableColumnBuffer.append(","+tableColumnList[j]+" bigint(22) DEFAULT 0 COMMENT '"+columnDescList.get(j)+"'");
				}else if(thisDataTypeList.get(j).equals("float")||thisDataTypeList.get(j).equals("Float")){
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(50) DEFAULT 0 COMMENT '"+columnDescList.get(j)+"'");
				}else if(thisDataTypeList.get(j).equals("short")||thisDataTypeList.get(j).equals("Short")){
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(22) DEFAULT 0 COMMENT '"+columnDescList.get(j)+"'");
				}else if(thisDataTypeList.get(j).equals("double")||thisDataTypeList.get(j).equals("Double")){
					tableColumnBuffer.append(","+tableColumnList[j]+" varchar(22) DEFAULT 0 COMMENT '"+columnDescList.get(j)+"'");
				}else if(thisDataTypeList.get(j).equals("byte[]")||thisDataTypeList.get(j).equals("Byte[]")){
					tableColumnBuffer.append(","+tableColumnList[j]+" mediumblob COMMENT '"+columnDescList.get(j)+"'");
				}
				
			}
			
		}
		String tableSQL1="CREATE TABLE "+tableName+"(";
		String columnSQL=tableColumnBuffer.toString().substring(1,tableColumnBuffer.toString().length())+",";
		String tableSQL3=" PRIMARY KEY ("+tableColumnList[0]+") ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;";
		String exeSQL=tableSQL1+columnSQL+tableSQL3;
		System.out.println("完整SQL语句："+exeSQL);
		pstmt=conn.prepareStatement(exeSQL);
		int A=pstmt.executeUpdate();
		return true;
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		if(pstmt!=null){
			try {
				pstmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
	}
	
	return false;
	
}

/**
 * 【得到某个类所有方法名】
 */
public static List<String> getObjectMethod(Object object){
	List<String> methodNameList=new ArrayList<String>();
	Method[] methods=object.getClass().getDeclaredMethods();
        for(Method method:methods){
        	methodNameList.add(method.getName());
        }
        return methodNameList;
}


/**
 * 【执行某个方法名称】【字符串方法名称】【返回有返回值】
 * @param methodName=方法名  object=方法所在类
 * @return Map<方法名,返回值> 
 */
public Map<String,Object> executeObjectMethod(Object object,String methodName){
	Map<String,Object> methodReturnValue=new HashMap<String, Object>();
	try{
		Method[] methods=object.getClass().getDeclaredMethods();
		for(Method method:methods){
	    	int paramSize =method.getParameterTypes().length;
	    	if(methodName.equals(method.getName())){
	    		methodReturnValue.put(method.getName(),method.invoke(object,new Object[paramSize]));
	    	}
	    	
	    }
	}catch(Exception e){
		e.printStackTrace();
	}
	return methodReturnValue;
}


/**
 * 【初始化myibatisXML】方法关键字【select ,insert ,update ,delte  】
 * @param args
 */
public static String getMyIbatisXML(Object object,Object mappObject,String tableName){
	StringBuffer buffer=new StringBuffer();
	try{
		buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">");
		buffer.append("\r\n");
		buffer.append("<mapper namespace=\""+mappObject.getClass().getPackage().toString().split(" ")[1]+"."+mappObject.getClass().getSimpleName()+"\">");
		buffer.append("\r\n");
		List<String> methodNameList=getObjectMethod(mappObject);
		for(String methodName:methodNameList){
			if(methodName.contains("insert")||methodName.contains("add")){
				buffer.append("<insert id=\""+methodName+"\" parameterType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\">");
				buffer.append("\r\n");
				buffer.append("insert into "+tableName+" ("+getObjectFieldsString(object,false)+")");
				buffer.append("\r\n");
				buffer.append("values ("+getMyibatisSymbol(object,false)+")");
				buffer.append("\r\n");
				buffer.append("</insert>");
				buffer.append("\r\n");
			}
			buffer.append("\r\n");
			if(methodName.contains("find")||methodName.contains("select")||methodName.contains("insert")||methodName.contains("init")){
				if(methodName.contains("count")||methodName.contains("num")||methodName.contains("number")){
					buffer.append("<select id=\""+methodName+"\" parameterType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\" resultType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\">");
					buffer.append("\r\n");
					buffer.append("select count(0) from "+tableName+" where 1=1");
					buffer.append("\r\n");
					for(String field:getObjectFieldsString(object,true).split(",")){
						buffer.append("<if test=\""+field+"!=null\"> and "+field+"=#{"+field+"}</if>");
						buffer.append("\r\n");
					}
					buffer.append("</select>");
					buffer.append("\r\n");
				}else{
					buffer.append("<select id=\""+methodName+"\" parameterType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\" resultType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\">");
					buffer.append("\r\n");
					buffer.append("select "+getObjectFieldsString(object,true)+" from "+tableName+" where 1=1");
					buffer.append("\r\n");
					for(String field:getObjectFieldsString(object,true).split(",")){
						buffer.append("<if test=\""+field+"!=null\"> and "+field+"=#{"+field+"}</if>");
						buffer.append("\r\n");
					}
					buffer.append(" order by create_time desc limit #{thisPage},#{pageSize}");
					buffer.append("\r\n");
					buffer.append("</select>");
					buffer.append("\r\n");
				}
			}
			buffer.append("\r\n");
			if(methodName.contains("delete")||methodName.contains("del")){
				buffer.append("<delete id=\""+methodName+"\" parameterType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\">");
				buffer.append("\r\n");
				buffer.append("delete from "+tableName+" where "+getObjectFieldsString(object,true).split(",")[0]+"=#{"+getObjectFieldsString(object,true).split(",")[0]+"}");
				buffer.append("\r\n");
				buffer.append("</delete>");
			}
			buffer.append("\r\n");
			if(methodName.contains("update")){
				buffer.append("<update id=\""+methodName+"\" parameterType=\""+object.getClass().getPackage().toString().split(" ")[1]+"."+object.getClass().getSimpleName()+"\">");
				buffer.append("\r\n");
				buffer.append("update "+tableName+" set "+getObjectFieldsString(object,true).split(",")[0]+"=#{"+getObjectFieldsString(object,true).split(",")[0]+"}");
				buffer.append("\r\n");
				for(String field:getObjectFieldsString(object,true).split(",")){
					buffer.append("<if test=\""+field+"!=null\">,"+field+"=#{"+field+"}</if>");
					buffer.append("\r\n");
				}
				buffer.append("\r\n");
				buffer.append("</update>");
				buffer.append("\r\n");
			}
		}
		buffer.append("\r\n");
		buffer.append("</mapper>");
		System.out.println(buffer.toString());
	}catch(Exception e){
		e.printStackTrace();
	}
	return buffer.toString();
}
	
public static void main(String[] args) {

//	List<String> columnDescList=new ArrayList<String>();
//	columnDescList.add("兑换产品信息数据标识");
//	columnDescList.add("兑换产品名称");
//	columnDescList.add("所需猿币数量");
//	columnDescList.add("剩余产品数量");
//	columnDescList.add("兑换产品状态【1、下架 2、上架】");
//	columnDescList.add("兑换产品状态【1、下架 2、上架】");
//	columnDescList.add("兑换产品状态【1、下架 2、上架】");
	System.out.println(ProwerfulObject.getObjectFieldsString(new TbVerifyTask(), false));
	System.out.println(ProwerfulObject.getDatabaseSymbol(new TbVerifyTask(), false)); 
	System.out.println(ProwerfulObject.getMyibatisSymbol(new TbVerifyTask(),false)); 
}
	
}
