package org.datagroup.dataAudit.common.po;

import java.io.Serializable;
import java.util.Date;
/**
 * 稽核结果实体类
 * @author wulijuan
 *
 */
public class TbVerifyRepair extends BaseEntity2 implements Serializable {
 
  private static final long serialVersionUID = 1L;
  private Integer id;
  private String src_table;
  private String siebel_row_id;
  private String target_table;
  private String status_cd;
  private Integer verify_type;
  private String verify_desc;
  private String error_msg;
  private Integer data_status;
  private String status_date;
  private Integer center_type;
  private String op_type;
  private String last_upd;
  private String create_time;
  private Integer limit;
  private String start_time;
  private String end_time;
  
public TbVerifyRepair() {
	super();
}
public TbVerifyRepair(Integer id, String src_table, String siebel_row_id,
		String target_table, String status_cd, Integer verify_type,
		String verify_desc, String error_msg, Integer data_status,
		String status_date, Integer center_type, String op_type,
		String last_upd, String create_time, Integer limit, String start_time,
		String end_time) {
	super();
	this.id = id;
	this.src_table = src_table;
	this.siebel_row_id = siebel_row_id;
	this.target_table = target_table;
	this.status_cd = status_cd;
	this.verify_type = verify_type;
	this.verify_desc = verify_desc;
	this.error_msg = error_msg;
	this.data_status = data_status;
	this.status_date = status_date;
	this.center_type = center_type;
	this.op_type = op_type;
	this.last_upd = last_upd;
	this.create_time = create_time;
	this.limit = limit;
	this.start_time = start_time;
	this.end_time = end_time;
}
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public String getSrc_table() {
	return src_table;
}
public void setSrc_table(String src_table) {
	this.src_table = src_table;
}
public String getSiebel_row_id() {
	return siebel_row_id;
}
public void setSiebel_row_id(String siebel_row_id) {
	this.siebel_row_id = siebel_row_id;
}
public String getTarget_table() {
	return target_table;
}
public void setTarget_table(String target_table) {
	this.target_table = target_table;
}
public String getStatus_cd() {
	return status_cd;
}
public void setStatus_cd(String status_cd) {
	this.status_cd = status_cd;
}
public Integer getVerify_type() {
	return verify_type;
}
public void setVerify_type(Integer verify_type) {
	this.verify_type = verify_type;
}
public String getVerify_desc() {
	return verify_desc;
}
public void setVerify_desc(String verify_desc) {
	this.verify_desc = verify_desc;
}
public String getError_msg() {
	return error_msg;
}
public void setError_msg(String error_msg) {
	this.error_msg = error_msg;
}
public Integer getData_status() {
	return data_status;
}
public void setData_status(Integer data_status) {
	this.data_status = data_status;
}
public String getStatus_date() {
	return status_date;
}
public void setStatus_date(String status_date) {
	this.status_date = status_date;
}
public Integer getCenter_type() {
	return center_type;
}
public void setCenter_type(Integer center_type) {
	this.center_type = center_type;
}
public String getOp_type() {
	return op_type;
}
public void setOp_type(String op_type) {
	this.op_type = op_type;
}
public String getLast_upd() {
	return last_upd;
}
public void setLast_upd(String last_upd) {
	this.last_upd = last_upd;
}
public String getCreate_time() {
	return create_time;
}
public void setCreate_time(String create_time) {
	this.create_time = create_time;
}
public Integer getLimit() {
	return limit;
}
public void setLimit(Integer limit) {
	this.limit = limit;
}
public String getStart_time() {
	return start_time;
}
public void setStart_time(String start_time) {
	this.start_time = start_time;
}
public String getEnd_time() {
	return end_time;
}
public void setEnd_time(String end_time) {
	this.end_time = end_time;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}


	
}
