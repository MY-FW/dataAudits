package org.datagroup.dataAudit.common.po;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class SiebelIncPojo implements Serializable{
	private String row_id;//数据唯一标识
	private String table_name;
	private String table_row_id;
	private String op_type;
	private String status_cd;//状态，70A待开始，70B稽核中，70C稽核完成，70E稽核异常
	private Timestamp create_date;
	private Timestamp status_date;
	private Timestamp last_upd;
	
	
	public SiebelIncPojo() {
		super();
	}
	public SiebelIncPojo(String row_id, String table_name, String table_row_id,
			String op_type, String status_cd, Timestamp create_date,
			Timestamp status_date, Timestamp last_upd) {
		super();
		this.row_id = row_id;
		this.table_name = table_name;
		this.table_row_id = table_row_id;
		this.op_type = op_type;
		this.status_cd = status_cd;
		this.create_date = create_date;
		this.status_date = status_date;
		this.last_upd = last_upd;
	}
	public String getRow_id() {
		return row_id;
	}
	public void setRow_id(String row_id) {
		this.row_id = row_id;
	}
	public String getTable_name() {
		return table_name;
	}
	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}
	public String getTable_row_id() {
		return table_row_id;
	}
	public void setTable_row_id(String table_row_id) {
		this.table_row_id = table_row_id;
	}
	public String getOp_type() {
		return op_type;
	}
	public void setOp_type(String op_type) {
		this.op_type = op_type;
	}
	public String getStatus_cd() {
		return status_cd;
	}
	public void setStatus_cd(String status_cd) {
		this.status_cd = status_cd;
	}
	public Timestamp getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}
	public Timestamp getStatus_date() {
		return status_date;
	}
	public void setStatus_date(Timestamp status_date) {
		this.status_date = status_date;
	}
	public Timestamp getLast_upd() {
		return last_upd;
	}
	public void setLast_upd(Timestamp last_upd) {
		this.last_upd = last_upd;
	}
	
	
}
